import http.client
import json
from typing import List


def send_seed_request(port, endpoint, dataset, api_key):
    conn = http.client.HTTPConnection("localhost:" + str(port))

    ids_of_created_objects: List[int] = []
    for data in dataset:
        encoded_data = json.dumps(data).encode('utf-8')
        headers = {
            'Authorization': 'Bearer ' + api_key,
            'Content-Type': 'application/json',
            'Content-Length': str(len(encoded_data))
        }
        conn.request("POST", endpoint, body=encoded_data, headers=headers)
        response = conn.getresponse()
        response_body = response.read()
        ids_of_created_objects.append(json.loads(response_body.decode("utf-8"))["id"])
        print(str(response.status) + " : " + response_body.decode("utf-8"))

    conn.close()
    return ids_of_created_objects

