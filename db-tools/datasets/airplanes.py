airplanes = [
    {"name": "Airplane 1", "type": "Boeing 747", "capacity": 196},
    {"name": "Airplane 2", "type": "Airbus A380", "capacity": 853},
    {"name": "Airplane 3", "type": "Boeing 787 Dreamliner", "capacity": 242},
    {"name": "Airplane 4", "type": "Airbus A320", "capacity": 180},
    {"name": "Airplane 5", "type": "Boeing 737", "capacity": 215},
    {"name": "Airplane 6", "type": "Embraer E190", "capacity": 114},
    {"name": "Airplane 7", "type": "Bombardier CRJ900", "capacity": 90},
    {"name": "Airplane 8", "type": "Airbus A350", "capacity": 440},
    {"name": "Airplane 9", "type": "Boeing 777", "capacity": 396},
    {"name": "Airplane 10", "type": "Airbus A330", "capacity": 335}
]
