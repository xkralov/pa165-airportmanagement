flights = []


def generate_flights(airplane_ids, destination_ids):
    start_times = ["2007-12-03T10:15:30+01:00", "2024-01-03T10:15:30+05:00", "2024-02-03T10:15:30+01:00", "2024-03-03T10:15:30+02:00",
                   "2024-04-03T10:15:30+02:00", "2024-05-03T10:15:30+02:00", "2024-06-03T10:15:30+02:00", "2024-07-03T10:15:30+02:00",
                   "2024-08-03T10:15:30+02:00", "2024-09-03T10:15:30+02:00"]
    end_times = ["2007-12-03T11:15:30+01:00", "2024-01-03T10:15:30+04:00", "2024-02-03T15:15:30+01:00", "2024-03-03T15:15:30+02:00",
                 "2024-04-03T15:15:30+02:00", "2024-05-03T15:15:30+02:00", "2024-06-03T15:15:30+02:00", "2024-07-03T15:15:30+02:00",
                 "2024-08-03T15:15:30+02:00", "2024-09-03T15:15:30+02:00"]
    for i in range(len(airplane_ids)):
        flights.append({"departureTime": start_times[i % len(start_times)],
                        "arrivalTime": end_times[i % len(end_times)],
                        "originDestinationId": destination_ids[i % len(destination_ids)],
                        "finalDestinationId": destination_ids[(i + 1) % len(destination_ids)],
                        "planeId": airplane_ids[i]})
