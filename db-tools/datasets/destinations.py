destinations = [
    {"city": "Prague", "country": "Czechia", "airportName": "Letiště Václava Havla"},
    {"city": "Paris", "country": "France", "airportName": "Charles de Gaulle Airport"},
    {"city": "New York City", "country": "United States", "airportName": "John F. Kennedy International Airport"},
    {"city": "Tokyo", "country": "Japan", "airportName": "Narita International Airport"},
    {"city": "London", "country": "United Kingdom", "airportName": "Heathrow Airport"},
    {"city": "Sydney", "country": "Australia", "airportName": "Sydney Airport"},
    {"city": "Dubai", "country": "United Arab Emirates", "airportName": "Dubai International Airport"},
    {"city": "Rome", "country": "Italy", "airportName": "Leonardo da Vinci–Fiumicino Airport"},
    {"city": "Singapore", "country": "Singapore", "airportName": "Changi Airport"},
    {"city": "Toronto", "country": "Canada", "airportName": "Toronto Pearson International Airport"}
]
