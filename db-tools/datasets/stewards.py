stewards = [
    {"firstName": "Steve", "lastName": "Harrington"},
    {"firstName": "Dirk", "lastName": "Gently"},
    {"firstName": "Annabeth", "lastName": "Chase"},
    {"firstName": "Lucy", "lastName": "Pevensie"},
    {"firstName": "Percy", "lastName": "Jackson"},
    {"firstName": "Joyce", "lastName": "Byers"},
    {"firstName": "Horac", "lastName": "Altman"},
    {"firstName": "Wednesday", "lastName": "Addams"},
    {"firstName": "Jane", "lastName": "Marplova"},
    {"firstName": "Anthony", "lastName": "Lockwood"}
]