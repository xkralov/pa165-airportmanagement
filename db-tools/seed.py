from seed_request import send_seed_request
from datasets.airplanes import airplanes
from datasets.destinations import destinations
from datasets.flights import flights, generate_flights
from datasets.stewards import stewards
from typing import List

if __name__ == "__main__":
    api_key: str = ""
    airplane_ids: List[int] = send_seed_request(8082, "/airplanes", airplanes, api_key)
    destination_ids: List[int] = send_seed_request(8082, "/destinations", destinations, api_key)
    generate_flights(airplane_ids, destination_ids)
    send_seed_request(8081, "/flights", flights, api_key)
    send_seed_request(8080, "/stewards", stewards, api_key)
