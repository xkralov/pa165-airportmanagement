package cz.muni.fi.pa165.resources.facade;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.mappers.AirplaneMapper;
import cz.muni.fi.pa165.resources.service.AirplaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneFacade {

    private final AirplaneService airplaneService;
    private final AirplaneMapper airplaneMapper;

    @Autowired
    public AirplaneFacade(AirplaneService airplaneService, AirplaneMapper airplaneMapper) {
        this.airplaneService = airplaneService;
        this.airplaneMapper = airplaneMapper;
    }

    public List<AirplaneDTO> listAirplanes() {
        return airplaneMapper.mapToList(airplaneService.listAirplanes());
    }

    public AirplaneDTO findById(Long id) {
        return airplaneMapper.mapToDto(airplaneService.findById(id));
    }

    public AirplaneDTO createAirplane(AirplaneDTO airplaneDTO) {
        return airplaneMapper.mapToDto(airplaneService.createAirplane(airplaneMapper.mapFromDto(airplaneDTO)));
    }

    public AirplaneDTO updateAirplane(AirplaneDTO airplaneDTO) {
        return airplaneMapper.mapToDto(airplaneService.updateAirplane(airplaneMapper.mapFromDto(airplaneDTO)));
    }

    public void deleteAirplane(Long id) {
        airplaneService.deleteAirplane(id);
    }
}
