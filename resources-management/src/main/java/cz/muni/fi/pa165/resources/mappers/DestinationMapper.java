package cz.muni.fi.pa165.resources.mappers;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.model.Destination;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DestinationMapper {
    DestinationDTO mapToDto(Destination destination);

    List<DestinationDTO> mapToList(List<Destination> destinations);

    Destination mapFromDto(DestinationDTO destinationDTO);

}
