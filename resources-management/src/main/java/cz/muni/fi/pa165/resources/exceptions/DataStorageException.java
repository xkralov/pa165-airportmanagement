package cz.muni.fi.pa165.resources.exceptions;

public class DataStorageException extends RuntimeException {
    public DataStorageException() {
    }

    public DataStorageException(String message) {
        super(message);
    }

    public DataStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataStorageException(Throwable cause) {
        super(cause);
    }

    public DataStorageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
