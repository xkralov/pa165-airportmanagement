package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.resources.model.Airplane;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirplaneRepository extends ListCrudRepository<Airplane, Long> {
}
