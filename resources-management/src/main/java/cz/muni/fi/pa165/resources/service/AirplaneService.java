package cz.muni.fi.pa165.resources.service;

import cz.muni.fi.pa165.resources.data.repository.AirplaneRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Airplane;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AirplaneService {

    private final AirplaneRepository airplaneRepository;

    private Validator validator;

    @Autowired
    public AirplaneService(AirplaneRepository airplaneRepository, Validator validator) {
        this.airplaneRepository = airplaneRepository;
        this.validator = validator;
    }

    public List<Airplane> listAirplanes() {
        return airplaneRepository.findAll();
    }

    public Airplane findById(Long id) {
        return airplaneRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Airplane with id: " + id + " was not found"));
    }

    public Airplane createAirplane(Airplane airplane) {
        validateAirplane(airplane);
        if (airplane.getId() != null) {
            airplane.setId(null);
        }

        return airplaneRepository.save(airplane);
    }

    public Airplane updateAirplane(Airplane airplane) {
        validateAirplane(airplane);
        Optional<Airplane> airplaneOptional = airplaneRepository.findById(airplane.getId());

        if (airplaneOptional.isPresent()) {
            return airplaneRepository.save(airplane);
        }

        throw new ResourceNotFoundException("Airplane with id: " + airplane.getId() + " was not found");
    }

    public void deleteAirplane(Long id) {
        Optional<Airplane> airplaneOptional = airplaneRepository.findById(id);

        if (airplaneOptional.isPresent()) {
            airplaneRepository.deleteById(id);
            return;
        }

        throw new ResourceNotFoundException("Airplane with id: " + id + " was not found");
    }

    private void validateAirplane(Airplane airplane) {
        Set<ConstraintViolation<Airplane>> violations = validator.validate(airplane);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<Airplane> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
                sb.append(", ");
            }
            throw new ConstraintViolationException("Invalid data: " + sb.toString(), violations);
        }
    }
}
