package cz.muni.fi.pa165.resources.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class Airplane {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column
    private Long id;
    @Column
    @NotNull(message = "Name must be provided")
    @NotBlank(message = "Name must not be blank")
    @NotEmpty(message = "Name must not be empty")
    @Size(min = 3, max = 30, message = "Length of airplane name must be between 3 and 20 letters")
    private String name;
    @Column
    @NotNull(message = "Type must be provided")
    @NotBlank(message = "Type must not be blank")
    @NotEmpty(message = "Type must not be empty")
    @Size(min = 1, max = 30, message = "Length of airplane type must be between 1 and 10 letters")
    private String type;
    @Column
    @NotNull(message = "Capacity must be provided")
    @Min(value = 2, message = "Airplane should have at least 2 seats")
    @Max(value = 1000, message = "Airplane should not have more than 1000 seats")
    private int capacity;

    @Override
    public String toString() {
        return "Airplane{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }
}
