package cz.muni.fi.pa165.resources.facade;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.mappers.DestinationMapper;
import cz.muni.fi.pa165.resources.service.DestinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinationFacade {

    private final DestinationService destinationService;
    private final DestinationMapper destinationMapper;

    @Autowired
    public DestinationFacade(DestinationService destinationService, DestinationMapper destinationMapper) {
        this.destinationService = destinationService;
        this.destinationMapper = destinationMapper;
    }

    public List<DestinationDTO> listDestinations() {
        return destinationMapper.mapToList(destinationService.listDestinations());
    }

    public DestinationDTO findById(Long id) {
        return destinationMapper.mapToDto(destinationService.findById(id));
    }

    public DestinationDTO createDestination(DestinationDTO destinationDTO) {
        return destinationMapper.mapToDto(destinationService.createDestination(destinationMapper.mapFromDto(destinationDTO)));
    }

    public DestinationDTO updateDestination(DestinationDTO destinationDTO) {
        return destinationMapper.mapToDto(destinationService.updateDestination(destinationMapper.mapFromDto(destinationDTO)));
    }

    public void deleteDestination(Long id) {
        destinationService.deleteDestination(id);
    }

}
