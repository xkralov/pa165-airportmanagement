package cz.muni.fi.pa165.resources.data.repository;

import cz.muni.fi.pa165.resources.model.Destination;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationRepository extends ListCrudRepository<Destination, Long> {
}
