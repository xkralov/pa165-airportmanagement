package cz.muni.fi.pa165.resources.controller;

import cz.muni.fi.pa165.generated.api.AirplanesApiDelegate;
import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.facade.AirplaneFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service
@RestController
@RequestMapping(path = "/airplanes")
public class AirplanesController implements AirplanesApiDelegate {

    private final AirplaneFacade airplaneFacade;

    @Autowired
    public AirplanesController(AirplaneFacade airplaneFacade) {
        this.airplaneFacade = airplaneFacade;
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Create new airplane"
    )
    @PostMapping
    public ResponseEntity<AirplaneDTO> createAirplane(@RequestBody AirplaneDTO airplaneDTO) {
        return new ResponseEntity<>(airplaneFacade.createAirplane(airplaneDTO), HttpStatus.CREATED);
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "List all airplanes"
    )
    @GetMapping
    public ResponseEntity<List<AirplaneDTO>> listAirplanes() {
        return ResponseEntity.ok(airplaneFacade.listAirplanes());
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Get airplane with id"
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<AirplaneDTO> getAirplane(@PathVariable("id") Long id) {
        return ResponseEntity.ok(airplaneFacade.findById(id));
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Update airplane with id"
    )
    @PutMapping(path = "/{id}")
    public ResponseEntity<AirplaneDTO> updateAirplane(@PathVariable Long id, @RequestBody AirplaneDTO airplaneDTO) {
        return ResponseEntity.ok(airplaneFacade.updateAirplane(airplaneDTO.id(id)));
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Delete airplane with id"
    )
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteAirplane(@PathVariable("id") Long id) {
        airplaneFacade.deleteAirplane(id);
        return ResponseEntity.noContent().build();
    }
}
