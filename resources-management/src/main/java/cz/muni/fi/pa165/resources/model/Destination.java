package cz.muni.fi.pa165.resources.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"country", "city", "airportName"})
})
public class Destination {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    @NotNull(message = "Country must be provided")
    @NotBlank(message = "Country must not be blank")
    @NotEmpty(message = "Country must not be empty")
    @Size(min = 3, max = 30, message = "Length of country name must be between 3 and 20 letters")
    private String country;
    @Column
    @NotNull(message = "City must be provided")
    @NotBlank(message = "City must not be blank")
    @NotEmpty(message = "City must not be empty")
    @Size(min = 3, max = 30, message = "Length of city name must be between 3 and 20 letters")
    private String city;
    @Column
    @NotNull(message = "Airport name must be provided")
    @NotBlank(message = "Airport name must not be blank")
    @NotEmpty(message = "Airport name must not be empty")
    @Size(min = 3, max = 30, message = "Length of airport name must be between 2 and 20 letters")
    private String airportName;

    @Override
    public String toString() {
        return "Destination{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", airportName='" + airportName + '\'' +
                '}';
    }
}
