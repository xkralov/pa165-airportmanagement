package cz.muni.fi.pa165.resources.mappers;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AirplaneMapper {

    AirplaneDTO mapToDto(Airplane airplane);

    List<AirplaneDTO> mapToList(List<Airplane> airplanes);

    Airplane mapFromDto(AirplaneDTO airplaneDTO);

}
