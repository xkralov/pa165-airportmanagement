package cz.muni.fi.pa165.resources.service;

import cz.muni.fi.pa165.resources.data.repository.DestinationRepository;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.model.Destination;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DestinationService {

    private final DestinationRepository destinationRepository;

    private Validator validator;

    @Autowired
    public DestinationService(DestinationRepository destinationRepository, Validator validator) {
        this.destinationRepository = destinationRepository;
        this.validator = validator;
    }

    public List<Destination> listDestinations() {
        return destinationRepository.findAll();
    }

    public Destination findById(Long id) {
        return destinationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Destination with id: " + id + " was not found."));
    }

    public Destination createDestination(Destination destination) {
        validateDestination(destination);
        if (destination.getId() != null) {
            destination.setId(null);
        }

        return destinationRepository.save(destination);
    }

    public Destination updateDestination(Destination destination) {
        validateDestination(destination);
        Optional<Destination> destinationOptional = destinationRepository.findById(destination.getId());

        if (destinationOptional.isPresent()) {
            return destinationRepository.save(destination);
        }

        throw new ResourceNotFoundException("Destination with id: " + destination.getId() + " was not found.");
    }

    public void deleteDestination(Long id) {
        Optional<Destination> destinationOptional = destinationRepository.findById(id);

        if (destinationOptional.isPresent()) {
            destinationRepository.deleteById(id);
            return;
        }

        throw new ResourceNotFoundException("Destination with id: " + id + " was not found.");
    }

    private void validateDestination(Destination destination) {
        Set<ConstraintViolation<Destination>> violations = validator.validate(destination);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<Destination> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
                sb.append(", ");
            }
            throw new ConstraintViolationException("Invalid data: " + sb.toString(), violations);
        }
    }
}
