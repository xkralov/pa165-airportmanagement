package cz.muni.fi.pa165.resources.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.data.repository.AirplaneRepository;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AirplanesControllerIT {
    private final Airplane TEST_AIRPLANE1 = new Airplane();
    private final Airplane TEST_AIRPLANE2 = new Airplane();

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private AirplaneRepository airplaneRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        TEST_AIRPLANE1.setId(1L);
        TEST_AIRPLANE1.setName("Airplane 1");
        TEST_AIRPLANE1.setType("Boeing 747");
        TEST_AIRPLANE1.setCapacity(100);

        TEST_AIRPLANE2.setId(2L);
        TEST_AIRPLANE2.setName("Airplane 2");
        TEST_AIRPLANE2.setType("Airbus A380");
        TEST_AIRPLANE2.setCapacity(200);
    }

    @Test
    void listAirplanes_returnsAirplanes() throws Exception {
        airplaneRepository.save(TEST_AIRPLANE1);
        airplaneRepository.save(TEST_AIRPLANE2);

        String response = mockMvc.perform(get("/airplanes"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<AirplaneDTO> entities = objectMapper.readValue(response, new TypeReference<>() {});

        assertThat(entities.size()).isEqualTo(2);

        assertThat(entities.get(0).getId()).isEqualTo(TEST_AIRPLANE1.getId());
        assertThat(entities.get(0).getName()).isEqualTo(TEST_AIRPLANE1.getName());
        assertThat(entities.get(0).getType()).isEqualTo(TEST_AIRPLANE1.getType());
        assertThat(entities.get(0).getCapacity()).isEqualTo(TEST_AIRPLANE1.getCapacity());

        assertThat(entities.get(1).getId()).isEqualTo(TEST_AIRPLANE2.getId());
        assertThat(entities.get(1).getName()).isEqualTo(TEST_AIRPLANE2.getName());
        assertThat(entities.get(1).getType()).isEqualTo(TEST_AIRPLANE2.getType());
        assertThat(entities.get(1).getCapacity()).isEqualTo(TEST_AIRPLANE2.getCapacity());
    }

}
