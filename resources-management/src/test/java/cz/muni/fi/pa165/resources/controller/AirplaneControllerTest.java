package cz.muni.fi.pa165.resources.controller;

import cz.muni.fi.pa165.generated.model.AirplaneDTO;
import cz.muni.fi.pa165.resources.facade.AirplaneFacade;
import cz.muni.fi.pa165.resources.model.Airplane;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AirplaneControllerTest {
    @Mock
    private AirplaneFacade airplaneFacade;

    @InjectMocks
    private AirplanesController airplanesController;

    private Airplane createAirplane() {
        Airplane airplane = new Airplane();
        airplane.setName("AirpName1");
        airplane.setType("AirplType1");
        airplane.setCapacity(100);
        return airplane;
    }

    private AirplaneDTO createAirplaneDto() {
        AirplaneDTO airplaneDTO = new AirplaneDTO();
        airplaneDTO.setName("AirpName1");
        airplaneDTO.setType("AirplType1");
        airplaneDTO.setCapacity(100);
        return airplaneDTO;
    }

    @Test
    void createAirplane_successful_returnsAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneFacade.createAirplane(airplaneDTO)).thenReturn(airplaneDTO);

        ResponseEntity<AirplaneDTO> response = airplanesController.createAirplane(airplaneDTO);

        assertThat(201).isEqualTo(response.getStatusCode().value());
        assertThat(airplaneDTO).isEqualTo(response.getBody());
        verify(airplaneFacade, times(1)).createAirplane(airplaneDTO);
    }

    @Test
    void listAirplanes_noAirplanesListed_returnsEmptyList() {
        when(airplaneFacade.listAirplanes()).thenReturn(new ArrayList<>());

        ResponseEntity<List<AirplaneDTO>> response = airplanesController.listAirplanes();

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEmpty();
        verify(airplaneFacade, times(1)).listAirplanes();
    }

    @Test
    void listAirplanes_successful_returnsAirplanes() {
        AirplaneDTO airplaneDTO1 = createAirplaneDto();
        AirplaneDTO airplaneDTO2 = createAirplaneDto();
        airplaneDTO2.setName("AirpName2");
        airplaneDTO2.setType("AirplType2");
        airplaneDTO2.setCapacity(200);

        List<AirplaneDTO> airplaneDTOS = List.of(airplaneDTO1, airplaneDTO2);

        when(airplaneFacade.listAirplanes()).thenReturn(airplaneDTOS);

        ResponseEntity<List<AirplaneDTO>> response = airplanesController.listAirplanes();

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(airplaneDTOS);
        verify(airplaneFacade, times(1)).listAirplanes();
    }

    @Test
    void getAirplane_successful_returnsAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneFacade.findById(1L)).thenReturn(airplaneDTO);

        ResponseEntity<AirplaneDTO> response = airplanesController.getAirplane(1L);

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(airplaneDTO);
        verify(airplaneFacade, times(1)).findById(anyLong());
    }

    @Test
    void updateAirplane_successful_returnsAirplane() {
        AirplaneDTO airplaneDTO = createAirplaneDto();
        when(airplaneFacade.updateAirplane(airplaneDTO)).thenReturn(airplaneDTO);

        ResponseEntity<AirplaneDTO> response = airplanesController.updateAirplane(1L, airplaneDTO);

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(airplaneDTO);
        verify(airplaneFacade, times(1)).updateAirplane(airplaneDTO);
    }

    @Test
    void deleteAirplane_successful_returnsNoContent() {
        doNothing().when(airplaneFacade).deleteAirplane(anyLong());

        ResponseEntity<Void> response = airplanesController.deleteAirplane(1L);

        assertThat(204).isEqualTo(response.getStatusCode().value());
        verify(airplaneFacade, times(1)).deleteAirplane(anyLong());
    }
}
