package cz.muni.fi.pa165.resources.controller;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.facade.DestinationFacade;
import cz.muni.fi.pa165.resources.model.Destination;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DestinationControllerTest {
    @Mock
    private DestinationFacade destinationFacade;

    @InjectMocks
    private DestinationsController destinationsController;

    private Destination createDestination() {
        Destination destination = new Destination();
        destination.setCountry("CountryName1");
        destination.setCity("CityName1");
        destination.setAirportName("airpName1");
        return destination;
    }

    private DestinationDTO createDestinationDto() {
        DestinationDTO destinationDto = new DestinationDTO();
        destinationDto.setCountry("CountryName1");
        destinationDto.setCity("CityName1");
        destinationDto.setAirportName("airpName1");
        return destinationDto;
    }

    @Test
    void createDestination_successful_returnsDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        when(destinationFacade.createDestination(destinationDTO)).thenReturn(destinationDTO);

        ResponseEntity<DestinationDTO> response = destinationsController.createDestination(destinationDTO);

        assertThat(201).isEqualTo(response.getStatusCode().value());
        assertThat(destinationDTO).isEqualTo(response.getBody());
        verify(destinationFacade, times(1)).createDestination(destinationDTO);
    }

    @Test
    void listDestinations_noDestinationsListed_returnsEmptyList() {
        when(destinationFacade.listDestinations()).thenReturn(new ArrayList<>());

        ResponseEntity<List<DestinationDTO>> response = destinationsController.listDestinations();

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEmpty();
        verify(destinationFacade, times(1)).listDestinations();
    }

    @Test
    void listDestinations_successful_returnsDestinations() {
        DestinationDTO destinationDTO1 = createDestinationDto();
        DestinationDTO destinationDTO2 = createDestinationDto();
        destinationDTO2.setCountry("CountryName2");
        destinationDTO2.setCity("CityName2");
        destinationDTO2.setAirportName("airpName2");
        List<DestinationDTO> destinationsDtos = List.of(destinationDTO1, destinationDTO2);

        when(destinationFacade.listDestinations()).thenReturn(destinationsDtos);

        ResponseEntity<List<DestinationDTO>> response = destinationsController.listDestinations();

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(destinationsDtos);
        verify(destinationFacade, times(1)).listDestinations();
    }

    @Test
    void getDestination_successful_returnsDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        when(destinationFacade.findById(1L)).thenReturn(destinationDTO);

        ResponseEntity<DestinationDTO> response = destinationsController.getDestination(1L);

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(destinationDTO);
        verify(destinationFacade, times(1)).findById(anyLong());
    }

    @Test
    void updateDestination_successful_returnsDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        when(destinationFacade.updateDestination(destinationDTO)).thenReturn(destinationDTO);

        ResponseEntity<DestinationDTO> response = destinationsController.updateDestination(1L, destinationDTO);

        assertThat(200).isEqualTo(response.getStatusCode().value());
        assertThat(response.getBody()).isEqualTo(destinationDTO);
        verify(destinationFacade, times(1)).updateDestination(destinationDTO);
    }

    @Test
    void deleteDestination_successful_returnsNoContent() {
        doNothing().when(destinationFacade).deleteDestination(anyLong());

        ResponseEntity<Void> response = destinationsController.deleteDestination(1L);

        assertThat(204).isEqualTo(response.getStatusCode().value());
        verify(destinationFacade, times(1)).deleteDestination(anyLong());
    }
}
