package cz.muni.fi.pa165.resources.facade;

import cz.muni.fi.pa165.generated.model.DestinationDTO;
import cz.muni.fi.pa165.resources.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.resources.mappers.DestinationMapper;
import cz.muni.fi.pa165.resources.model.Destination;
import cz.muni.fi.pa165.resources.service.DestinationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DestinationFacadeTest {
    @Mock
    private DestinationService destinationService;

    @Mock
    private DestinationMapper destinationMapper;

    @InjectMocks
    private DestinationFacade destinationFacade;

    private Destination createDestination() {
        Destination destination = new Destination();
        destination.setCountry("CountryName1");
        destination.setCity("CityName1");
        destination.setAirportName("airpName1");
        return destination;
    }

    private DestinationDTO createDestinationDto() {
        DestinationDTO destinationDto = new DestinationDTO();
        destinationDto.setCountry("CountryName1");
        destinationDto.setCity("CityName1");
        destinationDto.setAirportName("airpName1");
        return destinationDto;
    }

    @Test
    void listDestinations_noDestinationsListed_returnsEmptyList() {
        // Arrange
        when(destinationService.listDestinations()).thenReturn(new ArrayList<>());
        when(destinationMapper.mapToList(anyList())).thenReturn(new ArrayList<>());

        // Act
        List<DestinationDTO> result = destinationFacade.listDestinations();

        // Assert
        assertThat(result).isEmpty();
    }


    @Test
    void listDestinations_DestinationsExist_returnsMappedDestinations() {
        Destination destination1 = createDestination();
        Destination destination2 = createDestination();
        destination2.setCountry("CountryName2");
        destination2.setCity("CityName2");
        destination2.setAirportName("airpName2");

        DestinationDTO destinationDTO1 = createDestinationDto();
        DestinationDTO destinationDTO2 = createDestinationDto();
        destinationDTO2.setCountry("CountryName2");
        destinationDTO2.setCity("CityName2");
        destinationDTO2.setAirportName("airpName2");

        List<Destination> destinations = List.of(destination1, destination2);
        List<DestinationDTO> destinationsDtos = List.of(destinationDTO1, destinationDTO2);
        when(destinationService.listDestinations()).thenReturn(destinations);
        when(destinationMapper.mapToList(anyList())).thenReturn(destinationsDtos);

        List<DestinationDTO> result = destinationFacade.listDestinations();

        assertThat(result).isEqualTo(destinationsDtos);
        verify(destinationService).listDestinations();
        verify(destinationMapper).mapToList(anyList());
    }

    @Test
    void findById_destinationExists_returnsMappedDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        Destination destination = createDestination();

        when(destinationService.findById(1L)).thenReturn(destination);
        when(destinationMapper.mapToDto(destination)).thenReturn(destinationDTO);

        DestinationDTO result = destinationFacade.findById(1L);

        assertThat(result).isEqualTo(destinationDTO);
    }

    @Test
    void findById_destinationDoesNotExist_throwsResourceNotFoundException() {
        when(destinationService.findById(1L)).thenThrow(
                new ResourceNotFoundException("Destination with id: 1L was not found."));

        assertThrows(ResourceNotFoundException.class, () -> destinationFacade.findById(1L));
        verify(destinationService).findById(anyLong());
    }

    @Test
    void createDestination_successful_returnsMappedDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        Destination destination = createDestination();

        when(destinationMapper.mapToDto(destination)).thenReturn(destinationDTO);
        when(destinationMapper.mapFromDto(destinationDTO)).thenReturn(destination);
        when(destinationService.createDestination(destination)).thenReturn(destination);

        DestinationDTO result = destinationFacade.createDestination(destinationDTO);

        assertThat(result).isEqualTo(destinationDTO);
        verify(destinationService).createDestination(destination);
        verify(destinationMapper).mapToDto(destination);
    }

    @Test
    void updateDestination_destinationExists_returnsMappedDestination() {
        DestinationDTO destinationDTO = createDestinationDto();
        Destination destination = createDestination();
        when(destinationMapper.mapToDto(destination)).thenReturn(destinationDTO);
        when(destinationMapper.mapFromDto(destinationDTO)).thenReturn(destination);
        when(destinationService.updateDestination(destination)).thenReturn(destination);

        DestinationDTO result = destinationFacade.updateDestination(destinationDTO);

        assertThat(result).isEqualTo(destinationDTO);
        verify(destinationService).updateDestination(destination);
        verify(destinationMapper).mapToDto(destination);
    }

    @Test
    void updateDestination_serviceThrowsResourceNotFoundException_throwsRuntimeException() {
        DestinationDTO destinationDTO = createDestinationDto();
        Destination destination = createDestination();
        when(destinationService.updateDestination(destination))
                .thenThrow(new ResourceNotFoundException("Destination with id: " + 1L + " was not found"));

        assertThrows(RuntimeException.class, () -> destinationFacade.updateDestination(destinationDTO));
    }

    @Test
    void deleteDestination_successful_callsDeleteDestination() {
        doNothing().when(destinationService).deleteDestination(1L);

        destinationFacade.deleteDestination(1L);

        verify(destinationService).deleteDestination(anyLong());
    }
}
