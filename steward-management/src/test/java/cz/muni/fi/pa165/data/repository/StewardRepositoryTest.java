package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class StewardRepositoryTest {

    @Autowired
    private StewardRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void initData() {
        Steward steward = new Steward();
        steward.setId(1L);
        steward.setFirstName("FirstName1");
        steward.setLastName("LastName1");
        entityManager.merge(steward);
    }

    @Test
    void findStewardForFlight_stewardFound_returnsNonEmptyList() {
        List<Steward> result = repository.findStewardForFlight(OffsetDateTime.now(), OffsetDateTime.now().plusHours(4));
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getFirstName()).isEqualTo("FirstName1");
        assertThat(result.get(0).getLastName()).isEqualTo("LastName1");
    }
}
