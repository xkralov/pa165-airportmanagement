package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.data.repository.StewardShiftRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class StewardServiceTest {

    @Mock
    private StewardRepository stewardRepository;
    @Mock
    private StewardShiftRepository shiftRepository;

    @InjectMocks
    private StewardService stewardService;

    private int id = 1;

    Steward createTestSteward() {
        Steward steward = new Steward();
        steward.setFirstName("FName" + id);
        steward.setLastName("LName" + id);
        id++;
        return steward;
    }

    StewardShift createTestStewardShift() {
        StewardShift shift = new StewardShift();
        shift.setId((long) id);
        shift.setFlightId((long) id);
        shift.setDepartureTime(OffsetDateTime.parse("2024-01-11T14:30:00Z"));
        shift.setArrivalTime(OffsetDateTime.parse("2024-01-11T16:30:00Z"));
        id++;
        return shift;
    }

    @Test
    void createSteward_creationSuccessful_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.save(steward)).thenReturn(steward);

        // Act
        Steward returnedSteward = stewardService.createSteward(steward);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void findById_stewardFound_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = stewardService.findById(1L);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void findById_stewardNotFound_throwsException() {
        // Arrange
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.findById(1L));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void getAllStewards_noStewardsListed_returnsEmptyListOfStewards() {
        // Arrange
        Mockito.when(stewardRepository.findAll()).thenReturn(new ArrayList<>());

        // Act
        List<Steward> returnedStewards = stewardService.getAllStewards();

        // Assert
        assertThat(returnedStewards).isEmpty();
    }

    @Test
    void getAllStewards_someStewardsListed_returnsListOfStewards() {
        // Arrange
        Steward steward1 = createTestSteward();
        Steward steward2 = createTestSteward();
        List<Steward> stewards = List.of(steward1, steward2);
        Mockito.when(stewardRepository.findAll()).thenReturn(stewards);

        // Act
        List<Steward> returnedStewards = stewardService.getAllStewards();

        // Assert
        assertThat(returnedStewards).isEqualTo(stewards);
    }

    @Test
    void updateSteward_stewardFound_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        steward.setId(1L);
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.when(stewardRepository.save(steward)).thenReturn(steward);

        // Act
        Steward returnedSteward = stewardService.updateSteward(steward);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void updateSteward_stewardNotFound_throwsException() {
        // Arrange
        Steward steward = createTestSteward();
        steward.setId(1L);
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.updateSteward(steward));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(0)).save(steward);
    }

    @Test
    void deleteSteward_stewardFound_returnsVoid() {
        // Arrange
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.doNothing().when(stewardRepository).delete(steward);

        // Act
        stewardService.deleteSteward(1L);

        // Assert
        verify(stewardRepository, times(1)).delete(steward);
    }

    @Test
    void deleteSteward_stewardNotFound_throwsException() {
        // Arrange
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.deleteSteward(1L));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void assignStewardToFlight_stewardFound_returnsSteward() {
        // Arrange
        StewardShift stewardShift = createTestStewardShift();
        Steward steward = createTestSteward();
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.when(stewardRepository.findStewardForFlight(any(), any())).thenReturn(List.of(steward));
        Mockito.when(shiftRepository.findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
                any(Long.class), any(), any())).thenReturn(Optional.of(stewardShift));
        Mockito.when(stewardRepository.save(steward)).thenReturn(steward);

        // Act
        Steward returnedSteward = stewardService.assignStewardToFlight(stewardShift, 1L);
        steward.assignShift(stewardShift);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
    }

    @Test
    void assignStewardToFlight_stewardNotFound_throwsException() {
        // Arrange
        StewardShift stewardShift = new StewardShift();

        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.assignStewardToFlight(stewardShift, 1L));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void getFreeStewardsForFlight_stewardFound_returnsStewardsList() {
        // Arrange
        OffsetDateTime departTime = OffsetDateTime.now();
        OffsetDateTime arrivalTime = OffsetDateTime.now().plusHours(4);
        List<Steward> freeStewards = List.of(new Steward());
        Mockito.when(stewardRepository.findStewardForFlight(departTime, arrivalTime)).thenReturn(freeStewards);

        // Act
        List<Steward> returnedStewards = stewardService.getFreeStewardsForFlight(departTime, arrivalTime);

        // Assert
        assertThat(returnedStewards).isEqualTo(freeStewards);
    }

    @Test
    void removeStewardFromFlight_stewardRemoved_returnsSteward() {
        // Arrange
        Steward steward = createTestSteward();
        StewardShift stewardShift = createTestStewardShift();
        StewardShift otherStewardShift = createTestStewardShift();

        Set<StewardShift> stewardsShifts = new HashSet<>();
        Set<Steward> shiftsStewards = new HashSet<>();

        stewardsShifts.add(otherStewardShift);
        shiftsStewards.add(steward);

        steward.setAssignedShifts(stewardsShifts);
        otherStewardShift.setStewards(shiftsStewards);

        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.when(shiftRepository.findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
                stewardShift.getFlightId(), stewardShift.getDepartureTime(), stewardShift.getArrivalTime()))
                .thenReturn(Optional.of(otherStewardShift));
        Mockito.when(stewardRepository.save(steward)).thenReturn(steward);

        // Act
        Steward returnedStewards = stewardService.removeStewardFromFlight(1L, stewardShift);

        // Assert
        assertThat(returnedStewards).isEqualTo(steward);
    }

    @Test
    void removeStewardFromFlight_stewardNotFound_throwsEntityNotFoundException() {
        // Arrange
        StewardShift stewardShift = createTestStewardShift();

        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.removeStewardFromFlight(1L, stewardShift));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void removeStewardFromFlight_shiftNotFound_throwsEntityNotFoundException() {
        // Arrange
        Steward steward = createTestSteward();
        StewardShift stewardShift = createTestStewardShift();

        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.when(shiftRepository.findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
                        stewardShift.getFlightId(), stewardShift.getDepartureTime(), stewardShift.getArrivalTime()))
                .thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardService.removeStewardFromFlight(1L, stewardShift));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

}
