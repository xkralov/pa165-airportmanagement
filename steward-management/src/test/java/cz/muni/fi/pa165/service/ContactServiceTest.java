package cz.muni.fi.pa165.service;


import cz.muni.fi.pa165.data.model.Contact;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.repository.ContactRepository;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import cz.muni.fi.pa165.exceptions.StewardDataMismatchException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ContactServiceTest {

    @Mock
    private StewardRepository stewardRepository;

    @Mock
    private ContactRepository contactRepository;

    @InjectMocks
    private ContactService contactService;

    @Test
    void addContact_stewardFound_returnsSteward() {
        // Arrange
        Contact contact = new Contact();
        Steward steward = new Steward();
        Steward otherSteward = new Steward();
        otherSteward.addContact(contact);
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));
        Mockito.when(stewardRepository.save(Mockito.any(Steward.class))).thenReturn(steward);

        // Act
        Steward returnedSteward = contactService.addContact(1L, contact);

        // Assert
        assertThat(returnedSteward).isEqualTo(otherSteward);
    }

    @Test
    void addContact_stewardNotFound_throwsEntityNotFoundException() {
        // Arrange
        Contact contact = new Contact();

        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> contactService.addContact(1L, contact));

        // Assert
        Mockito.verify(stewardRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    void removeContact_contactFound_returnsVoid() {
        // Arrange
        Contact contact = new Contact();
        Steward steward = new Steward();
        steward.setId(1L);
        contact.setSteward(steward);
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.of(contact));
        Mockito.doNothing().when(contactRepository).delete(contact);

        // Act
        contactService.removeContact(1L, 2L);

        // Assert
        Mockito.verify(contactRepository, Mockito.times(1)).delete(contact);
    }

    @Test
    void removeContact_contactNotFound_throwsEntityNotFoundException() {
        // Arrange
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> contactService.removeContact(1L, 2L));

        // Assert
        Mockito.verify(contactRepository, Mockito.times(1)).findById(2L);
    }

    @Test
    void removeContact_stewardIdMismatch_throwsStewardDataMismatchException() {
        // Arrange
        Contact contact = new Contact();
        Steward steward = new Steward();
        steward.setId(3L);
        contact.setSteward(steward);
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.of(contact));

        // Act
        assertThrows(StewardDataMismatchException.class, () -> contactService.removeContact(1L, 2L));

        // Assert
        Mockito.verify(contactRepository, Mockito.times(1)).findById(2L);
    }

    @Test
    void updateContact_contactFound_returnsSteward() {
        // Arrange
        Contact contact = new Contact();
        contact.setId(2L);
        Steward steward = new Steward();
        steward.setId(1L);
        contact.setSteward(steward);
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.of(contact));
        Mockito.when(contactRepository.save(contact)).thenReturn(contact);
        Mockito.when(stewardRepository.findById(1L)).thenReturn(Optional.of(steward));

        // Act
        Steward returnedSteward = contactService.updateContact(1L, contact);

        // Assert
        assertThat(returnedSteward).isEqualTo(steward);
        Mockito.verify(contactRepository, Mockito.times(1)).save(contact);
    }

    @Test
    void updateContact_contactNotFound_throwsEntityNotFoundException() {
        // Arrange
        Contact contact = new Contact();
        contact.setId(2L);
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.empty());

        // Act
        assertThrows(EntityNotFoundException.class, () -> contactService.updateContact(1L, contact));

        // Assert
        Mockito.verify(contactRepository, Mockito.times(1)).findById(2L);
    }

    @Test
    void updateContact_stewardIdMismatch_throwsStewardDataMismatchException() {
        // Arrange
        Contact contact = new Contact();
        contact.setId(2L);
        Steward steward = new Steward();
        steward.setId(3L);
        contact.setSteward(steward);
        Mockito.when(contactRepository.findById(2L)).thenReturn(Optional.of(contact));

        // Act
        assertThrows(StewardDataMismatchException.class, () -> contactService.updateContact(1L, contact));

        // Assert
        Mockito.verify(contactRepository, Mockito.times(1)).findById(2L);
    }

}
