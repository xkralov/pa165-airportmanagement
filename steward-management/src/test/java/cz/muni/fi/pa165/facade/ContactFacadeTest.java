package cz.muni.fi.pa165.facade;


import cz.muni.fi.pa165.data.dto.ContactDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.model.Contact;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.mapper.ContactMapper;
import cz.muni.fi.pa165.mapper.StewardMapper;
import cz.muni.fi.pa165.service.ContactService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ContactFacadeTest {

    @Mock
    private ContactService contactService;

    @Mock
    private StewardMapper stewardMapper;

    @Mock
    private ContactMapper contactMapper;

    @InjectMocks
    private ContactFacade contactFacade;

    StewardDto stewardDto;
    Set<ContactDto> contactDtoSet;
    Set<Contact> contacts;

    @BeforeEach
    void setUp() {
        contacts = new HashSet<>();
        contactDtoSet = new HashSet<>();
        contactDtoSet.add(new ContactDto());
        contacts.add(new Contact());
        stewardDto = new StewardDto();
    }

    @Test
    void addContact_contactAdded_returnsStewardDto() {
        //Arrange
        ContactDto contactDto = new ContactDto();
        Contact contact = new Contact();
        Steward steward = new Steward();
        steward.setContacts(contacts);
        Mockito.when(contactMapper.mapToEntity(contactDto)).thenReturn(contact);
        Mockito.when(contactService.addContact(1L, contact)).thenReturn(steward);
        Mockito.when(contactMapper.mapToDtoList(contacts)).thenReturn(contactDtoSet);
        Mockito.when(stewardMapper.mapToDto(Mockito.any(Steward.class))).thenReturn(stewardDto);

        //Act
        StewardDto result = contactFacade.addContact(1L, contactDto);

        //Assert
        assertThat(result).isEqualTo(stewardDto);
    }

    @Test
    void removeContact_contactRemoved_returnsVoid() {
        //Arrange
        Steward steward = new Steward();
        steward.setContacts(contacts);
        Mockito.doNothing().when(contactService).removeContact(1L, 1L);

        //Act
        contactFacade.removeContact(1L, 1L);

        //Assert
        Mockito.verify(contactService, Mockito.times(1)).removeContact(1L, 1L);
    }

    @Test
    void updateContact_contactUpdated_returnsStewardDto() {
        //Arrange
        ContactDto contactDto = new ContactDto();
        Contact contact = new Contact();
        Steward steward = new Steward();
        steward.setContacts(contacts);
        Mockito.when(contactMapper.mapToEntity(contactDto)).thenReturn(contact);
        Mockito.when(contactService.updateContact(1L, contact)).thenReturn(steward);
        Mockito.when(contactMapper.mapToDtoList(contacts)).thenReturn(contactDtoSet);
        Mockito.when(stewardMapper.mapToDto(Mockito.any(Steward.class))).thenReturn(stewardDto);

        //Act
        StewardDto result = contactFacade.updateContact(1L, contactDto);

        //Assert
        assertThat(result).isEqualTo(stewardDto);
    }

}
