package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.facade.StewardFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class StewardControllerTest {

    @Mock
    private StewardFacade stewardFacade;

    @InjectMocks
    private StewardController stewardController;

    private int id = 1;

    StewardDto createTestStewardDTO() {
        StewardDto stewardDto = new StewardDto();
        stewardDto.setFirstName("FName" + id);
        stewardDto.setLastName("LName" + id);
        id++;
        return stewardDto;
    }

    @Test
    void createSteward_creationSuccessful_returnsResponseEntityCreated() {
        // Arrange
        CreateStewardDto createStewardDto = new CreateStewardDto();
        createStewardDto.setFirstName("FName1");
        createStewardDto.setLastName("LName1");
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.createSteward(createStewardDto)).thenReturn(stewardDto);

        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = stewardController.createSteward(createStewardDto);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

    @Test
    void findById_stewardFound_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.findById(1L)).thenReturn(stewardDto);

        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = stewardController.findById(1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

    @Test
    void getAllStewards_someStewardsListed_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto1 = createTestStewardDTO();
        StewardDto stewardDto2 = createTestStewardDTO();
        List<StewardDto> stewardDtoList = List.of(stewardDto1, stewardDto2);
        Mockito.when(stewardFacade.getAllStewards()).thenReturn(stewardDtoList);

        // Act
        ResponseEntity<List<StewardDto>> stewardDtoResponseEntity = stewardController.getAllStewards();

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDtoList);
    }

    @Test
    void updateSteward_updateSuccessful_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.updateSteward(stewardDto)).thenReturn(stewardDto);

        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = stewardController.updateSteward(stewardDto);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

    @Test
    void deleteSteward_deletionSuccessful_returnsResponseNoContent() {
        // Arrange
        Mockito.doNothing().when(stewardFacade).deleteSteward(1L);

        // Act
        ResponseEntity<Void> stewardDtoResponseEntity = stewardController.deleteSteward(1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    void assignStewardToFlight_assignSuccessful_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.assignStewardToFlight(1L,1L)).thenReturn(stewardDto);

        //Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = stewardController.assignStewardToFlight(1L, 1L);

        //Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

    @Test
    void getFreeStewardsForFlight_stewardFound_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.getFreeStewardsForFlight(1L)).thenReturn(List.of(stewardDto));
        // Act
        ResponseEntity<List<StewardDto>> stewardDtoResponseEntity = stewardController.getFreeStewardsForFlight(1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(List.of(stewardDto));
    }

    @Test
    void getFreeStewardsForFlight_stewardNotFound_returnsResponseEntityOk() {
        // Arrange
        List<StewardDto> emptyList = new ArrayList<>();
        Mockito.when(stewardFacade.getFreeStewardsForFlight(1L)).thenReturn(emptyList);
        // Act
        ResponseEntity<List<StewardDto>> stewardDtoResponseEntity = stewardController.getFreeStewardsForFlight(1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(emptyList);
    }

    @Test
    void removeStewardFromFlight_removalSuccessful_returnsResponseEntityOk() {
        // Arrange
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardFacade.removeStewardFromFlight(1L, 1L)).thenReturn(stewardDto);
        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = stewardController.removeStewardFromFlight(1L, 1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

}
