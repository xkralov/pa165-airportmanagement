package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.data.dto.ContactDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.facade.ContactFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ContactControllerTest {

    @Mock
    ContactFacade contactFacade;

    @InjectMocks
    ContactController contactController;

    @Test
    void addContactToSteward_contactAdded_returnsResponseEntityCreated() {
        // Arrange
        ContactDto contactDto = new ContactDto();
        StewardDto stewardDto = new StewardDto();
        Mockito.when(contactFacade.addContact(1L, contactDto)).thenReturn(stewardDto);

        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = contactController.addContactToSteward(1L, contactDto);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

    @Test
    void removeContactFromSteward_contactRemoved_returnsNoContent() {
        // Arrange
        StewardDto stewardDto = new StewardDto();
        Mockito.doNothing().when(contactFacade).removeContact(1L, 1L);

        // Act
        ResponseEntity<Void> stewardDtoResponseEntity = contactController.removeContactFromSteward(1L, 1L);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    void updateStewardsContact_contactUpdated_returnsResponseEntityOk() {
        // Arrange
        ContactDto contactDto = new ContactDto();
        StewardDto stewardDto = new StewardDto();
        Mockito.when(contactFacade.updateContact(1L, contactDto)).thenReturn(stewardDto);

        // Act
        ResponseEntity<StewardDto> stewardDtoResponseEntity = contactController.updateStewardsContact(1L, contactDto);

        // Assert
        assertThat(stewardDtoResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(stewardDtoResponseEntity.getBody()).isEqualTo(stewardDto);
    }

}
