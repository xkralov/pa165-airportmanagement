package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class StewardControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StewardRepository stewardRepository;

    @Test
    void findStewardById_returnsSteward() throws Exception {
        Steward steward = new Steward();
        steward.setId(1L);
        steward.setFirstName("FirstName");
        steward.setLastName("LastName");
        stewardRepository.save(steward);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/stewards/1"))

                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("FirstName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("LastName"));
    }

}
