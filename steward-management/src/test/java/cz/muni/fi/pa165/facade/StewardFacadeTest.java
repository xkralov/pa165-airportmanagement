package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import cz.muni.fi.pa165.mapper.StewardMapper;
import cz.muni.fi.pa165.service.FlightExternalService;
import cz.muni.fi.pa165.service.StewardService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class StewardFacadeTest {

    @Mock
    private StewardService stewardService;

    @Mock
    private StewardMapper stewardMapper;

    @Mock
    private FlightExternalService flightExternalService;

    @InjectMocks
    private StewardFacade stewardFacade;

    private int id = 1;

    Steward createTestSteward() {
        Steward steward = new Steward();
        steward.setFirstName("FName" + id);
        steward.setLastName("LName" + id);
        id++;
        return steward;
    }

    StewardDto createTestStewardDTO() {
        StewardDto stewardDto = new StewardDto();
        stewardDto.setFirstName("FName" + id);
        stewardDto.setLastName("LName" + id);
        id++;
        return stewardDto;
    }

    @Test
    void createSteward_creationSuccessful_returnsStewardDTO() {
        // Arrange
        CreateStewardDto createStewardDto = new CreateStewardDto();
        createStewardDto.setFirstName("FName1");
        createStewardDto.setLastName("LName1");

        Steward steward = createTestSteward();
        StewardDto stewardDto = createTestStewardDTO();

        Mockito.when(stewardService.createSteward(steward)).thenReturn(steward);
        Mockito.when(stewardMapper.mapToDto(steward)).thenReturn(stewardDto);
        Mockito.when(stewardMapper.mapToEntity(createStewardDto)).thenReturn(steward);

        // Act
        StewardDto returnedStewardDto = stewardFacade.createSteward(createStewardDto);

        // Assert
        assertThat(returnedStewardDto).isEqualTo(stewardDto);
    }


    @Test
    void findById_stewardFound_returnsStewardDTO() {
        // Arrange
        Steward steward = createTestSteward();
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardService.findById(1L)).thenReturn(steward);
        Mockito.when(stewardMapper.mapToDto(steward)).thenReturn(stewardDto);

        // Act
        StewardDto returnedStewardDto = stewardFacade.findById(1L);

        // Assert
        assertThat(returnedStewardDto).isEqualTo(stewardDto);
    }

    @Test
    void findById_stewardNotFound_throwsException() {
        // Arrange
        Mockito.when(stewardService.findById(1L)).thenThrow(new EntityNotFoundException());

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardFacade.findById(1L));

        // Assert
        verify(stewardService, times(1)).findById(1L);
    }

    @Test
    void getAllStewards_someStewardsListed_returnsListOfStewardDtos() {
        // Arrange
        Steward steward1 = createTestSteward();
        Steward steward2 = createTestSteward();

        StewardDto stewardDto1 = createTestStewardDTO();
        StewardDto stewardDto2 = createTestStewardDTO();

        List<Steward> stewards = List.of(steward1, steward2);
        List<StewardDto> stewardDtos = List.of(stewardDto1, stewardDto2);

        Mockito.when(stewardService.getAllStewards()).thenReturn(stewards);
        Mockito.when(stewardMapper.mapToList(stewards)).thenReturn(stewardDtos);

        // Act
        List<StewardDto> returnedStewardDtos = stewardFacade.getAllStewards();

        // Assert
        assertThat(returnedStewardDtos).isEqualTo(stewardDtos);
    }

    @Test
    void updateSteward_stewardFound_returnsStewardDTO() {
        // Arrange
        Steward steward = createTestSteward();
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardService.updateSteward(steward)).thenReturn(steward);
        Mockito.when(stewardMapper.mapToDto(steward)).thenReturn(stewardDto);
        Mockito.when(stewardMapper.mapToEntity(stewardDto)).thenReturn(steward);

        // Act
        StewardDto returnedStewardDto = stewardFacade.updateSteward(stewardDto);

        // Assert
        assertThat(returnedStewardDto).isEqualTo(stewardDto);
    }

    @Test
    void updateSteward_stewardNotFound_throwsException() {
        // Arrange
        Steward steward = createTestSteward();
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(stewardService.updateSteward(steward)).thenThrow(new EntityNotFoundException());
        Mockito.when(stewardMapper.mapToEntity(stewardDto)).thenReturn(steward);

        // Act
        assertThrows(EntityNotFoundException.class, () -> stewardFacade.updateSteward(stewardDto));

        // Assert
        verify(stewardService, times(1)).updateSteward(steward);
    }

    @Test
    void deleteSteward_stewardFound_returnsVoid() {
        // Arrange
        Mockito.doNothing().when(stewardService).deleteSteward(1L);

        // Act
        stewardFacade.deleteSteward(1L);

        // Assert
        verify(stewardService, times(1)).deleteSteward(1L);
    }

    @Test
    void deleteSteward_stewardNotFound_throwsException() {
        // Arrange
        assertThrows(Exception.class, () -> {
            doThrow().when(stewardService).deleteSteward(1L);
        });

        // Act
        stewardFacade.deleteSteward(1L);

        // Assert
        verify(stewardService, times(1)).deleteSteward(1L);
    }

    @Test
    void assignStewardToFlight_stewardFound_returnsStewardDto() {
        // Arrange
        FlightDto flightDto = new FlightDto();
        StewardShift stewardShift = new StewardShift();
        Steward steward = createTestSteward();
        steward.setId(1L);
        StewardDto stewardDto = createTestStewardDTO();
        Mockito.when(flightExternalService.getFlightById(1L)).thenReturn(flightDto);
        Mockito.when(stewardMapper.mapToEntity(flightDto)).thenReturn(stewardShift);
        Mockito.when(stewardService.assignStewardToFlight(stewardShift, 1L)).thenReturn(steward);
        Mockito.when(stewardMapper.mapToDto(steward)).thenReturn(stewardDto);

        // Act
        StewardDto returnedStewardDto = stewardFacade.assignStewardToFlight(1L, 1L);

        // Assert
        assertThat(returnedStewardDto).isEqualTo(stewardDto);
    }

    @Test
    void getFreeStewardsForFlight_stewardFound_returnsStewardDtoList() {
        // Arrange
        FlightDto flightDto = new FlightDto();
        flightDto.setDepartureTime(OffsetDateTime.now());
        flightDto.setArrivalTime(OffsetDateTime.now().plusHours(4));
        List<StewardDto> stewardDtoList = List.of(new StewardDto());
        List<Steward> stewardList = List.of(createTestSteward());
        Mockito.when(flightExternalService.getFlightById(1L)).thenReturn(flightDto);
        Mockito.when(stewardMapper.mapToList(stewardList)).thenReturn(stewardDtoList);
        Mockito.when(stewardService.getFreeStewardsForFlight(flightDto.getDepartureTime(), flightDto.getArrivalTime())).thenReturn(stewardList);

        // Act
        List<StewardDto> returnedStewardDtoList = stewardFacade.getFreeStewardsForFlight(1L);

        // Assert
        assertThat(returnedStewardDtoList).isEqualTo(stewardDtoList);
    }

    @Test
    void removeStewardsFromFlight_stewardRemoved_returnsStewardDto() {
        // Arrange
        FlightDto flightDto = new FlightDto();
        StewardShift stewardShift = new StewardShift();
        StewardDto stewardDto = new StewardDto();
        Steward steward = createTestSteward();
        Mockito.when(flightExternalService.getFlightById(1L)).thenReturn(flightDto);
        Mockito.when(stewardMapper.mapToEntity(flightDto)).thenReturn(stewardShift);
        Mockito.when(stewardMapper.mapToDto(steward)).thenReturn(stewardDto);
        Mockito.when(stewardService.removeStewardFromFlight(1L, stewardShift)).thenReturn(steward);

        // Act
        StewardDto returnedStewardDto = stewardFacade.removeStewardFromFlight(1L, 1L);

        // Assert
        assertThat(returnedStewardDto).isEqualTo(stewardDto);
    }

}
