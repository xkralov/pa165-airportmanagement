package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.StewardShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.Optional;

@Repository
public interface StewardShiftRepository extends JpaRepository<StewardShift, Long> {
    Optional<StewardShift> findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
            Long flightId, OffsetDateTime departureTime, OffsetDateTime arrivalTime);
}
