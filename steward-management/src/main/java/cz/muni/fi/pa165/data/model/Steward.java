package cz.muni.fi.pa165.data.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table
public class Steward {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "steward_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "steward_shift",
            joinColumns = @JoinColumn(name = "steward_id"),
            inverseJoinColumns = @JoinColumn(name = "shift_id")
    )
    private Set<StewardShift> assignedShifts = new HashSet<>();

    @OneToMany(mappedBy = "steward",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.MERGE},
            orphanRemoval = true)
    private Set<Contact> contacts = new HashSet<>();


    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public boolean removeContact(Contact contact) {
        return contacts.remove(contact);
    }

    public void assignShift(StewardShift shift) {
        this.assignedShifts.add(shift);
    }

    public boolean unassignShift(StewardShift shift) {
        return assignedShifts.remove(shift);
    }

    @Override
    public String toString() {
        return "Steward{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", assignedShifts=[ " + assignedShifts.stream().map(shift -> " " + shift.getId()) +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Steward s)) {
            return false;
        }

        return this.id == s.id;
    }
}
