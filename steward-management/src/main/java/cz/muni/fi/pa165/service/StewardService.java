package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.data.repository.StewardShiftRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import cz.muni.fi.pa165.exceptions.StewardDataMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class StewardService {
    private final StewardRepository stewardRepository;
    private final StewardShiftRepository shiftRepository;

    @Autowired
    public StewardService(StewardRepository stewardRepository, StewardShiftRepository shiftRepository) {
        this.stewardRepository = stewardRepository;
        this.shiftRepository = shiftRepository;
    }

    public Steward createSteward(Steward stewardToSave) {
        return stewardRepository.save(stewardToSave);
    }

    public Steward findById(Long id) {
        Optional<Steward> stewardOptional = stewardRepository.findById(id);
        if (stewardOptional.isEmpty()) {
            throw new EntityNotFoundException(id);
        }
        return stewardOptional.get();
    }

    public List<Steward> getAllStewards() {
        return stewardRepository.findAll();
    }

    public Steward updateSteward(Steward stewardUpdated) {
        Optional<Steward> stewardUpdatedOptional = stewardRepository.findById(stewardUpdated.getId());
        if (stewardUpdatedOptional.isEmpty()) {
            throw new EntityNotFoundException(stewardUpdated.getId());
        }

        stewardUpdated.setContacts(stewardUpdatedOptional.get().getContacts());
        stewardUpdated.setAssignedShifts(stewardUpdatedOptional.get().getAssignedShifts());
        return stewardRepository.save(stewardUpdated);
    }

    public void deleteSteward(Long id) {
        Optional<Steward> stewardToDeleteOptional = stewardRepository.findById(id);
        if (stewardToDeleteOptional.isEmpty()) {
            throw new EntityNotFoundException(id);
        }
        stewardRepository.delete(stewardToDeleteOptional.get());
    }

    public Steward assignStewardToFlight(StewardShift shift, Long stewardId) {
        Optional<Steward> stewardOpt = stewardRepository.findById(stewardId);
        if (stewardOpt.isEmpty()) {
            throw new EntityNotFoundException(stewardId);
        }
        Steward steward = stewardOpt.get();

        List<Steward> freeStewards = stewardRepository.findStewardForFlight(
                shift.getDepartureTime(), shift.getArrivalTime());
        if (freeStewards.isEmpty()) {
            throw new EntityNotFoundException("No free stewards for given flight");
        }
        if (!freeStewards.contains(steward)) {
            throw new StewardDataMismatchException(stewardId, shift.getFlightId(),
                    shift.getDepartureTime(), shift.getArrivalTime());
        }

        Optional<StewardShift> dbShiftOptional = shiftRepository
                .findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
                        shift.getFlightId(), shift.getDepartureTime(), shift.getArrivalTime());
        StewardShift stewardShift = shift;
        if (dbShiftOptional.isPresent()) {
            stewardShift = dbShiftOptional.get();
        }

        steward.assignShift(stewardShift);
        stewardShift.addStewardToShift(steward);
        shiftRepository.save(stewardShift);
        stewardRepository.save(steward);

        return stewardRepository.findById(stewardId).get();
    }

    public List<Steward> getFreeStewardsForFlight(OffsetDateTime departureTime, OffsetDateTime arrivalTime) {
        return stewardRepository.findStewardForFlight(departureTime, arrivalTime);
    }

    public Steward removeStewardFromFlight(Long stewardId, StewardShift requestShift) {
        Optional<Steward> dbStewardOpt = stewardRepository.findById(stewardId);
        if (dbStewardOpt.isEmpty()) {
            throw new EntityNotFoundException(stewardId);
        }

        Optional<StewardShift> dbShiftOpt = shiftRepository
                .findStewardShiftByFlightIdAndDepartureTimeAndArrivalTime(
                        requestShift.getFlightId(), requestShift.getDepartureTime(), requestShift.getArrivalTime()
                );
        if (dbShiftOpt.isEmpty()) {
            throw new EntityNotFoundException("Shift not found");
        }

        Steward steward = dbStewardOpt.get();
        StewardShift shift = dbShiftOpt.get();
        boolean shiftUnassigned = steward.unassignShift(shift);
        boolean stewardUnassigned = shift.removeStewardFromShift(steward);
        if (!shiftUnassigned || !stewardUnassigned) {
            throw new StewardDataMismatchException(stewardId, requestShift.getFlightId());
        }

        shiftRepository.save(shift);
        return stewardRepository.save(steward);
    }

}
