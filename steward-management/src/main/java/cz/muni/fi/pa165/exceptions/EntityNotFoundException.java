package cz.muni.fi.pa165.exceptions;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException() {
        super("Entity not found");
    }

    public EntityNotFoundException(Long id) {
        super("Steward with id " + id.toString() + " not found");
    }

    public EntityNotFoundException(String type, Long id) {
        super(type + " with id " + id.toString() + " not found");
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

}
