package cz.muni.fi.pa165.mapper;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.dto.StewardShiftDto;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@Mapper(componentModel = "spring")
public interface StewardMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "flightId", source = "id")
    StewardShift mapToEntity(FlightDto flightDto);

    StewardShift mapToEntity(StewardShiftDto shiftDto);

    StewardShiftDto mapToDto(StewardShift shift);
    Set<StewardShiftDto> mapToDtoList(Set<StewardShift> shifts);

    StewardDto mapToDto(Steward steward);

    Steward mapToEntity(StewardDto stewardDto);

    Steward mapToEntity(CreateStewardDto stewardDto);

    List<StewardDto> mapToList(List<Steward> airplanes);
}
