package cz.muni.fi.pa165.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "shift")
public class StewardShift {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "shift_id")
    private Long id;

    @Column(name = "flight_id")
    private Long flightId;

    @Column(name = "departure_time")
    private OffsetDateTime departureTime;

    @Column(name = "arrival_time")
    private OffsetDateTime arrivalTime;

    @ManyToMany(mappedBy = "assignedShifts")
    private Set<Steward> stewards = new HashSet<>();

    public void addStewardToShift(Steward steward) {
        this.stewards.add(steward);
    }

    public boolean removeStewardFromShift(Steward steward) {
        return stewards.remove(steward);
    }

    @Override
    public String toString() {
        return "StewardShift{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", stewards=" + stewards.stream().map(steward -> " " + steward.getId()) +
                '}';
    }
}
