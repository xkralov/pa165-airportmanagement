package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
public class FlightExternalService implements FlightExternalServiceInterface {
    private static final String FLIGHT_MANAGEMENT_ADDRESS = "http://flight-service:8081/";
    private static final String FLIGHT_PATH = "flights";

    @Override
    public FlightDto getFlightById(Long flightId) {
        String uri = FLIGHT_MANAGEMENT_ADDRESS + FLIGHT_PATH + '/' + flightId;
        RestClient restClient = RestClient.create();
        FlightDto flightDtoResponseEntity = restClient.get()
                .uri(uri)
                .header("Authorization", getAuthorizationHeader())
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, (request, response) -> {
                    throw new EntityNotFoundException("Flight", flightId);
                })
                .body(FlightDto.class);

        return flightDtoResponseEntity;
    }

    private String getAuthorizationHeader() {
        OAuth2AccessToken accessToken = (OAuth2AccessToken) SecurityContextHolder.getContext()
                .getAuthentication()
                .getCredentials();
        return "Bearer " + accessToken.getTokenValue();
    }
}
