package cz.muni.fi.pa165.data.model;

public enum ContactType {
    EMAIL,
    MOBILE,
    ADDRESS
}
