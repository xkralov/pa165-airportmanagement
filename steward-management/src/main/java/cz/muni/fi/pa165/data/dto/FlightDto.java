package cz.muni.fi.pa165.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
public class FlightDto {
    private Long id;

    private OffsetDateTime departureTime;

    private OffsetDateTime arrivalTime;

    private Long originDestinationId;

    private Long finalDestinationId;

    private Long planeId;

    private List<Long> stewardIds;

    @Override
    public String toString() {
        return "Flight {" +
                "id=" + id +
                ", departureTime='" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", originDestination=" + originDestinationId +
                ", finalDestination=" + finalDestinationId +
                ", plane=" + planeId +
                ", stewards=" + stewardIds +
                "}";
    }
}
