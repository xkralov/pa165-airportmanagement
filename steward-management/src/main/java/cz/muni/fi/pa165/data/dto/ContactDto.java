package cz.muni.fi.pa165.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactDto {

    @NotNull
    @Schema(description = "Automatically generated identifier")
    private Long id;

    @Pattern(regexp = "EMAIL|MOBILE|ADDRESS", message = "Contact type should be EMAIL, MOBILE or ADDRESS")
    @Schema(description = "Type of contact", example = "EMAIL")
    private String contactType;

    @NotNull(message = "Contact must be specified")
    @NotBlank(message = "Contact cannot be blank")
    @NotEmpty(message = "Contact cannot be empty")
    @Schema(description = "Contact on steward", example = "john@doe.com")
    private String contact;

}
