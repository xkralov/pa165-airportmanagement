package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.data.dto.ContactDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.exceptions.ApiError;
import cz.muni.fi.pa165.facade.ContactFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name = "Contact", description = "Controller for management of contacts on stewards ")
@RestController
@RequestMapping(path = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactController {
    private final ContactFacade contactFacade;

    @Autowired
    public ContactController(ContactFacade contactFacade) {
        this.contactFacade = contactFacade;
    }


    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Add new contact on steward",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Contact on steward added",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = StewardDto.class)
                                    )}),
                    @ApiResponse(responseCode = "404", description = "Steward with given ID was not found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ApiError.class)
                                    )})
            }
    )
    @Parameter(description = "ID of edited steward", in = ParameterIn.PATH, name = "stewardId",
            schema = @Schema(type = "integer", format = "int64"))
    @PostMapping("/{stewardId}")
    public ResponseEntity<StewardDto> addContactToSteward(@PathVariable("stewardId") Long stewardId,
                                                          @Valid @RequestBody ContactDto contact) {
        return new ResponseEntity<>(contactFacade.addContact(stewardId, contact), HttpStatus.CREATED);
    }

    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Remove contact on steward",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Contact removed"),
                    @ApiResponse(responseCode = "404", description = "Steward with given id was not found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ApiError.class)
                                    )}),
                    @ApiResponse(responseCode = "409", description = "Contact with contactId does not belong to steward with stewardId",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ApiError.class)
                                    )})
            },
            parameters = {
                    @Parameter(name = "stewardId", description = "ID of edited steward",
                            in = ParameterIn.PATH ,
                            content = @Content(schema = @Schema(type = "integer", format = "int64"))),
                    @Parameter(name = "contactId", description = "ID of contact to be deleted",
                            in = ParameterIn.PATH ,
                            content = @Content(schema = @Schema(type = "integer", format = "int64")))
            }
    )
    @DeleteMapping("/{stewardId}/{contactId}")
    public ResponseEntity<Void> removeContactFromSteward(@PathVariable("stewardId") Long stewardId,
                                                               @PathVariable("contactId") Long contactId) {
        contactFacade.removeContact(stewardId, contactId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Update contact on steward",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Contact updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = StewardDto.class)
                                    )}),
                    @ApiResponse(responseCode = "404", description = "Steward with given ID was not found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ApiError.class)
                                    )}),
                    @ApiResponse(responseCode = "409", description = "Contact with given id does not belong to steward with stewardId",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ApiError.class)
                                    )})
            }
    )
    @Parameter(description = "ID of steward whose contact will be updated", in = ParameterIn.PATH, name = "stewardId",
            schema = @Schema(type = "integer", format = "int64"))
    @PutMapping("/{stewardId}")
    public ResponseEntity<StewardDto> updateStewardsContact(@PathVariable("stewardId") Long stewardId,
                                                            @Valid @RequestBody ContactDto contact) {
        return new ResponseEntity<>(contactFacade.updateContact(stewardId, contact), HttpStatus.OK);
    }
}
