package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.data.dto.CreateStewardDto;
import cz.muni.fi.pa165.data.dto.FlightDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.model.StewardShift;
import cz.muni.fi.pa165.mapper.StewardMapper;
import cz.muni.fi.pa165.service.FlightExternalServiceInterface;
import cz.muni.fi.pa165.service.StewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StewardFacade {
    private final StewardService stewardService;
    private final FlightExternalServiceInterface externalService;
    private final StewardMapper stewardMapper;

    @Autowired
    public StewardFacade(StewardService stewardService, StewardMapper stewardMapper, FlightExternalServiceInterface externalService) {
        this.stewardService = stewardService;
        this.externalService = externalService;
        this.stewardMapper = stewardMapper;
    }

    public StewardDto createSteward(CreateStewardDto stewardDto) {
        Steward stewardToSave = stewardMapper.mapToEntity(stewardDto);
        return stewardMapper.mapToDto(stewardService.createSteward(stewardToSave));
    }

    public StewardDto findById(Long id) {
        return stewardMapper.mapToDto(stewardService.findById(id));
    }

    public List<StewardDto> getAllStewards() {
        return stewardMapper.mapToList(stewardService.getAllStewards());
    }

    public StewardDto updateSteward(StewardDto stewardToUpdateDto) {
        Steward stewardToUpdate = stewardMapper.mapToEntity(stewardToUpdateDto);
        return stewardMapper.mapToDto(stewardService.updateSteward(stewardToUpdate));
    }

    public void deleteSteward(Long id) {
        stewardService.deleteSteward(id);
    }

    public StewardDto assignStewardToFlight(Long flightId, Long stewardId) {
        FlightDto flightDto = externalService.getFlightById(flightId);
        StewardShift shift = stewardMapper.mapToEntity(flightDto);
        Steward assignedSteward = stewardService.assignStewardToFlight(shift, stewardId);
        return stewardMapper.mapToDto(assignedSteward);
    }

    public List<StewardDto> getFreeStewardsForFlight(Long flightId) {
        FlightDto flightDto = externalService.getFlightById(flightId);
        return stewardMapper.mapToList(
                stewardService.getFreeStewardsForFlight(flightDto.getDepartureTime(), flightDto.getArrivalTime()));
    }

    public StewardDto removeStewardFromFlight(Long stewardId, Long flightId) {
        FlightDto flightDto = externalService.getFlightById(flightId);
        StewardShift shift = stewardMapper.mapToEntity(flightDto);
        Steward editedSteward = stewardService.removeStewardFromFlight(stewardId, shift);
        return stewardMapper.mapToDto(editedSteward);
    }

}
