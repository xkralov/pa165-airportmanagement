package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.data.dto.ContactDto;
import cz.muni.fi.pa165.data.dto.StewardDto;
import cz.muni.fi.pa165.data.model.Contact;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.mapper.ContactMapper;
import cz.muni.fi.pa165.mapper.StewardMapper;
import cz.muni.fi.pa165.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ContactFacade {
    private final ContactService contactService;
    private final ContactMapper contactMapper;
    private final StewardMapper stewardMapper;

    @Autowired
    public ContactFacade(ContactService contactService, ContactMapper contactMapper, StewardMapper stewardMapper) {
        this.contactService = contactService;
        this.contactMapper = contactMapper;
        this.stewardMapper = stewardMapper;
    }

    public StewardDto addContact(Long stewardId, ContactDto contactDto) {
        Contact contact = contactMapper.mapToEntity(contactDto);
        Steward updatedSteward = contactService.addContact(stewardId, contact);
        return mapStewardWithContacts(updatedSteward);
    }

    public void removeContact(Long stewardId, Long contactId) {
        contactService.removeContact(stewardId, contactId);
    }

    public StewardDto updateContact(Long stewardId, ContactDto contactDto) {
        Contact contact = contactMapper.mapToEntity(contactDto);
        Steward updatedSteward = contactService.updateContact(stewardId, contact);
        return mapStewardWithContacts(updatedSteward);
    }

    private StewardDto mapStewardWithContacts(Steward updatedSteward) {
        Set<ContactDto> contactDtos = contactMapper.mapToDtoList(updatedSteward.getContacts());
        StewardDto updatedStewardDto = stewardMapper.mapToDto(updatedSteward);
        updatedStewardDto.setContacts(contactDtos);
        return updatedStewardDto;
    }
}
