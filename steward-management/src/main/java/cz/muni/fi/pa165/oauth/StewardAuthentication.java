package cz.muni.fi.pa165.oauth;

import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.web.SecurityFilterChain;
import org.springframework.stereotype.Component;


@Component
public class StewardAuthentication {

    private static final String SECURITY_SCHEME_BEARER = "Bearer";

    @Bean
    SecurityFilterChain stewardsSecurityFilterChain(final HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.POST, "/stewards").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/stewards/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/stewards").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.PUT, "/stewards").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.DELETE, "/stewards/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PUT, "/stewards/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.POST, "/contacts/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.DELETE, "/contacts/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PUT, "/contacts/**").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll())
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }

    @Bean
    public OpenApiCustomizer stewardsOpenAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes(SECURITY_SCHEME_BEARER,
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    );
        };
    }

}
