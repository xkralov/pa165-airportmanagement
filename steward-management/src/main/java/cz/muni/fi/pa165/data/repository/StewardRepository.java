package cz.muni.fi.pa165.data.repository;

import cz.muni.fi.pa165.data.model.Steward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface StewardRepository extends JpaRepository<Steward, Long> {
    @Query("""
        select
            s
        from
            Steward s
        where s not in (
            select
                ss.stewards
            from
                StewardShift ss
            where
                ss.id in (
                    select
                        sshift.id
                    from
                        StewardShift sshift
                    where
                        sshift.departureTime between :departureTime and :arrivalTime
                        or :departureTime between sshift.departureTime and sshift.arrivalTime
                        or :arrivalTime between sshift.departureTime and sshift.arrivalTime
                        or (
                            sshift.departureTime between :departureTime and :arrivalTime
                            and sshift.arrivalTime between :departureTime and :arrivalTime
                        )
                )
            )
        """)
    List<Steward> findStewardForFlight(OffsetDateTime departureTime, OffsetDateTime arrivalTime);
}
