package cz.muni.fi.pa165.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class StewardShiftDto {

    private Long id;

    private Long flightId;

    private OffsetDateTime departureTime;

    private OffsetDateTime arrivalTime;

}
