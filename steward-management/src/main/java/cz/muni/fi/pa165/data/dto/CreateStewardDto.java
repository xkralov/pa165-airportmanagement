package cz.muni.fi.pa165.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateStewardDto {

    @NotNull(message = "First name must be specified")
    @NotBlank(message = "First name cannot be blank")
    @NotEmpty(message = "First name cannot be empty")
    @Size(min = 3, max = 30, message = "Length of first name must be between 3 and 30 letters")
    @Schema(description = "Steward's first name", example = "John")
    private String firstName;

    @NotNull(message = "Last name must be specified")
    @NotBlank(message = "Last name cannot be blank")
    @NotEmpty(message = "Last name cannot be empty")
    @Size(min = 3, max = 30, message = "Length of last name must be between 3 and 30 letters")
    @Schema(description = "Steward's last name", example = "Doe")
    private String lastName;

}
