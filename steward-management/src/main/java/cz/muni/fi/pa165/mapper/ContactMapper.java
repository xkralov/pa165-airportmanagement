package cz.muni.fi.pa165.mapper;

import cz.muni.fi.pa165.data.dto.ContactDto;
import cz.muni.fi.pa165.data.model.Contact;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring")
public interface ContactMapper {
    ContactDto mapToDto(Contact contact);
    Contact mapToEntity(ContactDto contactDto);
    Set<Contact> mapToEntityList(Set<ContactDto> dtoContacts);
    Set<ContactDto> mapToDtoList(Set<Contact> contacts);
}
