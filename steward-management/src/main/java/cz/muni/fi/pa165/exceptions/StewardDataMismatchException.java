package cz.muni.fi.pa165.exceptions;

import java.time.OffsetDateTime;

public class StewardDataMismatchException extends RuntimeException {

    public StewardDataMismatchException() {
        super("Steward's data mismatch");
    }

    public StewardDataMismatchException(Long stewardId, Long flightId) {
        super("Steward with id " + stewardId.toString() + " is not assigned to flight with id " + flightId.toString());
    }

    public StewardDataMismatchException(Long stewardId, Long flightId, OffsetDateTime departure,
                                        OffsetDateTime arrival) {
        super("Steward with id " + stewardId.toString() +
                " has another flight planned which collides with timing of selected flight with id "+ flightId +
                " (departure: " + departure +  ", arrival: " + arrival + ")"
        );
    }

    public StewardDataMismatchException(String message) {
        super(message);
    }
}
