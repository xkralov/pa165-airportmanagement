package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.dto.FlightDto;
import org.springframework.stereotype.Service;

public interface FlightExternalServiceInterface {
    FlightDto getFlightById(Long flightId);
}
