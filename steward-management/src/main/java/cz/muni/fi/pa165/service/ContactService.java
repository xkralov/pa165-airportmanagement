package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.data.model.Contact;
import cz.muni.fi.pa165.data.model.Steward;
import cz.muni.fi.pa165.data.repository.ContactRepository;
import cz.muni.fi.pa165.data.repository.StewardRepository;
import cz.muni.fi.pa165.exceptions.EntityNotFoundException;
import cz.muni.fi.pa165.exceptions.StewardDataMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {
    private final StewardRepository stewardRepository;
    private final ContactRepository contactRepository;

    @Autowired
    public ContactService(StewardRepository stewardRepository, ContactRepository contactRepository) {
        this.stewardRepository = stewardRepository;
        this.contactRepository = contactRepository;
    }


    public Steward addContact(Long stewardId, Contact contact) {
        Optional<Steward> dbStewardOpt = stewardRepository.findById(stewardId);
        if (dbStewardOpt.isEmpty()) {
            throw new EntityNotFoundException(stewardId);
        }

        Steward steward = dbStewardOpt.get();
        if (contact.getId() != null) {
            contact.setId(null);
        }
        steward.addContact(contact);
        contact.setSteward(steward);
        contact.setId(-1L);

        return stewardRepository.save(steward);
    }

    public void removeContact(Long stewardId, Long contactId) {
        Optional<Contact> dbContactDto = contactRepository.findById(contactId);
        if (dbContactDto.isEmpty()) {
            throw new EntityNotFoundException("Contact", contactId);
        }

        Contact contact = dbContactDto.get();
        if (contact.getSteward().getId() != stewardId) {
            String msg = "Contact with id " + contactId + " does not belong to steward with id " + stewardId;
            throw new StewardDataMismatchException(msg);
        }

        contactRepository.delete(contact);
    }

    public Steward updateContact(Long stewardId, Contact requestContact) {
        Optional<Contact> dbContactOpt = contactRepository.findById(requestContact.getId());
        if (dbContactOpt.isEmpty()) {
            throw new EntityNotFoundException("Contact", requestContact.getId());
        }
        Contact dbContact = dbContactOpt.get();
        if (dbContact.getSteward().getId() != stewardId) {
            String msg = "Contact with id " + requestContact.getId() +
                    " does not belong to steward with id " + stewardId;
            throw new StewardDataMismatchException(msg);
        }

        requestContact.setSteward(dbContact.getSteward());
        contactRepository.save(requestContact);
        return stewardRepository.findById(stewardId).get();
    }

}
