# Airport Management System

Our project aims to develop an information system for managing flight records at an airport. This system facilitates the entry, update, and deletion of records pertaining to stewards, airplanes, and destinations. It allows for the recording of flights, ensuring validity by checking airplane availability and steward assignment. Airport managers can utilize features such as CRUD operations for flights, stewards, airplanes, and destinations. Customer service functionalities include filtering and ordering flights based on various criteria.

### Run application

- In root directory call `docker-compose up`
- It will run all microservices and databases

#### Api key

- Running application with docker-compose will start confidential client using port 8083
- Open url localhost:8083 
- Click `Login MUNI` and choose privileges
- Identifier of user is required, test_read is necessary to access protected get endpoints, test_write to access post, delete, put and patch endpoints
- After pressing `YES, CONTINUE` you should see access token

#### Database seed

- To seed database, you need to have running application
- In directory db-tools open seed.py and enter valid api key to variable at line 9 with at least test_write scope
- Then run `python3 seed.py` in db-tools directory

### Data classes

- Destination: airport location (country, city)
- Airplane: capacity of the plane, its name and optionally its type
- Steward: first name, last name
- Flight: departure and arrival times, the origin, the destination and the plane

### Services

1. Flight management service
    - Runs on port 8081
    - Supports CRUD operations for Flights
    - Possibility to list flights with filtering according to some parameters
    - Checks that airplane and destination exists to create flight
    - Checks that airplane does not fly before creating new flight with that airplane

2. Steward management service 
    - Runs on port 8080
    - Supports CRUD operations for Stewards
    - Managing schedule and availability of Stewards
    - Managing which steward will serve which flight
    - Storing 0..n contact information for each Steward

3. Resource management service
    - runs on port 8082
    - Supports CRUD operations for Airplanes 
    - Supports CRUD operations for Destinations

### Observability
- Prometheus
  - running on <http://localhost:9090>
- Grafana
  - running on <http://localhost:3000>
  - username: `admin`
  - password: `admin`

### Runnable Scenario
The goal of scenario is to show basic usage of Airport Management system. 
For ordinary user the most common and only allowed operations are listing flights and destinations. 
Another scenario is example of usage of the system by an admin.
For admin, we assume that the most frequently used operations would be creation of flights and assignment 
of stewards to the flights. These operations require to have created at least 2 destinations, 1 airport and 1 steward.

How to run the scenario:
1. you need to have Python 3 installed
2. switch to the directory: `cd runnable-scenario`
3. create virtual environment: `python3 -m venv <venv_name>`
4. install requirements: `pip install -r requirements.txt`
    - if there is a problem with pywin32 you can try installing only Locust package with `pip3 install locust`
5. Add your authentication token into the `AUTH_TOKEN` variable declared on the top of the `locustfile.py`
6. run Locust with command: `locust`
   - in case `locust` doesn't work, try using `python -m locust` 
6. you can see the UI at port `8089 (http://localhost:8089)` 

### Use Case
![Airport management system UC diagram](UseCase.png)

### Class Diagram
![Airport management system class diagram](ClassDiagram.png)