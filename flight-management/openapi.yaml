openapi: 3.0.3
info:
  title: Flight Management microservice OpenAPI definition
  description: Flight Management microservice OpenAPI definition
  version: 1.0.0
servers:
  - url: 'http://localhost:8081'
tags:
  - name: FlightManagement
components:
  schemas:
    FlightDTO:
      title: A flight
      description: Flight object
      required:
        - departureTime
        - arrivalTime
        - originDestinationId
        - finalDestinationId
        - planeId
      properties:
        id:
          type: integer
          format: int64
        departureTime:
          type: string
          format: date-time
          description: Time when flight departs
          example: 2024-12-31T12:30:00Z
        arrivalTime:
          type: string
          format: date-time
          description: Time when flight arrives
          example: 2024-12-31T14:30:00Z
        originDestinationId:
          type: integer
          format: int64
          description: Id of location from which airplane departs
          example: 1
        finalDestinationId:
          type: integer
          format: int64
          description: Id of location to which airplane arrives
          example: 2
        planeId:
          type: integer
          format: int64
          description: Id of plane for flight
          example: 1
paths:
  /flights:
    get:
      tags:
        - Flights
      summary: Returns an array of all Flights
      operationId: listFlights
      parameters:
        - name: departureAfter
          in: query
          required: false
          schema:
            type: string
            format: date-time
          description: Listed flights will depart after this date-time
        - name: arrivalBefore
          in: query
          required: false
          schema:
            type: string
            format: date-time
          description: Listed flights will arrive to final destination before this date-time
        - name: departureDestination
          in: query
          required: false
          schema:
            type: integer
            format: int64
          description: Listed flights will depart from this destination
        - name: finalDestination
          in: query
          required: false
          schema:
            type: integer
            format: int64
          description: Listed flights will arrive to this destination
        - name: plane
          in: query
          required: false
          schema:
            type: integer
            format: int64
          description: Listed flights will be with selected airplane
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/FlightDTO'
    post:
      security:
        - Bearer:
            - test_write
      tags:
        - Flights
      summary: Creates a Flight
      operationId: createFlight
      requestBody:
        required: false
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FlightDTO'
      responses:
        "201":
          description: Flight successfully created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FlightDTO'
        "400":
          description: Unable to create flight
  /flights/{id}:
    get:
      tags:
        - Flights
      summary: Retrieve a single Flight
      operationId: getFlight
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
          description: "ID of the flight to retrieve"
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FlightDTO'
        "404":
          description: Flight not found
    put:
      security:
        - Bearer:
            - test_write
      tags:
        - Flights
      summary: Updates an existing Flight
      operationId: updateFlight
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
          description: "ID of the flight to update"
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FlightDTO'
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FlightDTO'
        "404":
          description: Flight not found
    delete:
      security:
        - Bearer:
            - test_write
      tags:
        - Flights
      summary: Deletes an existing Flight
      operationId: deleteFlight
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
          description: "ID of the flight to delete"
      responses:
        "204":
          description: No content
        "404":
          description: Flight not found