package cz.muni.fi.pa165.flights.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Setter
@Getter
@Entity
@Table
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column
    private Long id;

    @Column
    private OffsetDateTime departureTime;

    @Column
    private OffsetDateTime arrivalTime;

    @Column
    private Long originDestinationId;

    @Column
    private Long finalDestinationId;

    @Column
    private Long planeId;

    @Override
    public String toString() {
        return "Flight {" +
        "id=" + id +
        ", departureTime='" + departureTime +
        ", arrivalTime=" + arrivalTime +
        ", originDestination=" + originDestinationId +
        ", finalDestination=" + finalDestinationId +
        ", plane=" + planeId +
        "}";
    }
}
