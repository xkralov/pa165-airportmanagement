package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.flights.data.repository.FlightRepository;
import cz.muni.fi.pa165.flights.exceptions.DataStorageException;
import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class FlightService {

    private final FlightRepository flightRepository;
    private final ExternalServiceClient externalServiceClient;
    public static final OffsetDateTime MIN_DATETIME = OffsetDateTime.parse("0000-01-01T00:00:00Z");
    public static final OffsetDateTime MAX_DATETIME = OffsetDateTime.parse("9999-12-31T23:59:59Z");

    @Autowired
    public FlightService(FlightRepository flightRepository,
                         ExternalServiceClient externalServiceClient) {
        this.flightRepository = flightRepository;
        this.externalServiceClient = externalServiceClient;
    }

    public List<Flight> listFlights(@Nullable OffsetDateTime departureAfter,
                                    @Nullable OffsetDateTime arrivalBefore,
                                    @Nullable Long departureDestination,
                                    @Nullable Long finalDestination,
                                    @Nullable Long plane) {
        if (departureAfter == null) {
            departureAfter = MIN_DATETIME;
        }
        if (arrivalBefore == null) {
            arrivalBefore = MAX_DATETIME;
        }
        return flightRepository.findAllByAttributes(departureAfter, arrivalBefore, departureDestination, finalDestination, plane);
    }

    public Flight findById(Long id) {
        return flightRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Flight with id: " + id + " was not found"));
    }

    public Flight createFlight(Flight flight) {
        verifyParameters(flight);
        isPlaneAvailable(flight);
        if (flight.getId() != null) {
            flight.setId(null);
        }
        return flightRepository.save(flight);
    }

    public Flight updateFlight(Flight flight) {
        verifyParameters(flight);

        Optional<Flight> optionalFlight = flightRepository.findById(flight.getId());

        if (optionalFlight.isPresent()) {
            isPlaneAvailable(flight);
            return flightRepository.save(flight);
        } else {
            throw new ResourceNotFoundException("Flight with id: " + flight.getId() + " was not found");
        }
    }

    public void deleteFlight(Long id) {
        Optional<Flight> optionalFlight = flightRepository.findById(id);
        if (optionalFlight.isEmpty()) {
            throw new ResourceNotFoundException("Flight with id: " + id + " was not found");
        }
        flightRepository.deleteById(id);
    }

    private void verifyParameters(Flight flight) {

        Long planeId = flight.getPlaneId();
        externalServiceClient.airplaneExists(planeId);

        Long originDestId = flight.getOriginDestinationId();
        externalServiceClient.destinationExists(originDestId);

        Long finalDestId = flight.getFinalDestinationId();
        externalServiceClient.destinationExists(finalDestId);

        if (originDestId.equals(finalDestId)) {
            throw new DataStorageException("Final destination cannot be same as starting destination");
        }

        if (!flight.getDepartureTime().isBefore(flight.getArrivalTime())) {
            throw new DataStorageException("Departure time must be before arrival time");
        }
    }

    private void isPlaneAvailable(Flight flight) {
        List<Flight> overlappingFlights =
                flightRepository.findOverlappingFlights(flight.getDepartureTime(), flight.getArrivalTime(), flight.getPlaneId(), flight.getId());
        if (!overlappingFlights.isEmpty()) {
            throw new DataStorageException("Plane with id: " + flight.getPlaneId() + " is not available for flight from " + flight.getDepartureTime() + " to " + flight.getArrivalTime());
        };
    }

}
