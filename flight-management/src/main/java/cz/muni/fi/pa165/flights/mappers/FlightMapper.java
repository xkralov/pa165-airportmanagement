package cz.muni.fi.pa165.flights.mappers;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FlightMapper {
    FlightDTO mapToDto(Flight flight);

    List<FlightDTO> mapToList(List<Flight> flights);

    Flight mapToEntity(FlightDTO flightDTO);

    default Page<FlightDTO> mapToPageDto(Page<Flight> flights) {
        return new PageImpl<>(mapToList(flights.getContent()), flights.getPageable(), flights.getTotalPages());
    }
}
