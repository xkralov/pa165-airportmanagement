package cz.muni.fi.pa165.flights.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Airplane {

    private Long id;
    private String name;
    private String type;
    private int capacity;

    @Override
    public String toString() {
        return "Airplane{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }

}
