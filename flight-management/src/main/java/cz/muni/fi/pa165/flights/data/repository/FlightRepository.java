package cz.muni.fi.pa165.flights.data.repository;

import cz.muni.fi.pa165.flights.model.Flight;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {

    @Query("SELECT f FROM Flight f WHERE" +
            "( :planeId IS null OR f.planeId = :planeId ) AND " +
            "( :originId IS null OR f.originDestinationId = :originId ) AND " +
            "( :finalDestId IS null OR f.finalDestinationId = :finalDestId ) AND " +
            "( f.departureTime >= :departureTime ) AND " +
            "( f.arrivalTime <= :arrivalTime )")
    List<Flight> findAllByAttributes(@NotNull @Param("departureTime") OffsetDateTime departureTime,
                                     @NotNull @Param("arrivalTime") OffsetDateTime arrivalTime,
                                     @Nullable @Param("originId") Long originId,
                                     @Nullable @Param("finalDestId") Long finalDestId,
                                     @Nullable @Param("planeId") Long planeId);

    @Query("SELECT f FROM Flight f WHERE " +
            "( f.id != :flightId ) AND " +
            "( f.planeId = :planeId ) AND " +
            "( f.departureTime <= :arrivalTime ) AND " +
            "( f.arrivalTime >= :departureTime )")
    List<Flight> findOverlappingFlights(@NotNull @Param("departureTime") OffsetDateTime departureTime,
                                        @NotNull @Param("arrivalTime") OffsetDateTime arrivalTime,
                                        @NotNull @Param("planeId") Long planeId,
                                        @Nullable @Param("flightId") Long flightId);

}
