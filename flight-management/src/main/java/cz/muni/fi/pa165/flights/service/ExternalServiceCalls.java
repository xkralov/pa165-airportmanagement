package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.exceptions.DataStorageException;
import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.flights.model.Airplane;
import cz.muni.fi.pa165.flights.model.Destination;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
public class ExternalServiceCalls implements ExternalServiceClient {

    private static final String ADDRESS = "http://resources-service:8082/";
    private static final String AIRPLANE_PATH = "airplanes";
    private static final String DESTINATION_PATH = "destinations";

    public void airplaneExists(Long id) {
        String uri = ADDRESS + AIRPLANE_PATH + '/' + id;
        RestClient restClient = RestClient.create();
        Airplane airplaneResponseEntity = restClient.get()
                .uri(uri)
                .header("Authorization", getAuthorizationHeader())
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, (request, response) -> {
                    throw new ResourceNotFoundException("Airplane with id: " + id + " does not exist");
                })
                .body(Airplane.class);
    }

    public void destinationExists(Long id) {
        String uri = ADDRESS + DESTINATION_PATH + '/' + id;
        RestClient restClient = RestClient.create();
        Destination destinationResponseEntity = restClient.get()
                .uri(uri)
                .header("Authorization", getAuthorizationHeader())
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, (request, response) -> {
                    throw new ResourceNotFoundException("Destination with id: " + id + " does not exist");
                })
                .body(Destination.class);
    }

    private String getAuthorizationHeader() {
        OAuth2AccessToken accessToken = (OAuth2AccessToken) SecurityContextHolder.getContext()
                .getAuthentication()
                .getCredentials();
        return "Bearer " + accessToken.getTokenValue();
    }

}
