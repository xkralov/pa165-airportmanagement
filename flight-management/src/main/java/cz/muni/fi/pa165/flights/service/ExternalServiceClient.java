package cz.muni.fi.pa165.flights.service;

public interface ExternalServiceClient {

    void airplaneExists(Long id);

    void destinationExists(Long id);

}
