package cz.muni.fi.pa165.flights.facade;

import cz.muni.fi.pa165.flights.service.FlightService;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import cz.muni.fi.pa165.flights.mappers.FlightMapper;
import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class FlightFacade {

    private final FlightService flightService;
    private final FlightMapper flightMapper;

    @Autowired
    public FlightFacade(FlightService flightService, FlightMapper flightMapper) {
        this.flightService = flightService;
        this.flightMapper = flightMapper;
    }

    public List<FlightDTO> listFlights(@Nullable OffsetDateTime departureAfter,
                                       @Nullable OffsetDateTime arrivalBefore,
                                       @Nullable Long departureDestination,
                                       @Nullable Long finalDestination,
                                       @Nullable Long plane) {
        return flightMapper.mapToList(flightService.listFlights(departureAfter, arrivalBefore, departureDestination, finalDestination, plane));
    }

    public FlightDTO findById(Long id) {
        return flightMapper.mapToDto(flightService.findById(id));
    }

    public FlightDTO createFlight(FlightDTO flightDTO) {
        return flightMapper.mapToDto(flightService.createFlight(flightMapper.mapToEntity(flightDTO)));
    }

    public FlightDTO updateFlight(FlightDTO flightDTO) {
        return flightMapper.mapToDto(flightService.updateFlight(flightMapper.mapToEntity(flightDTO)));
    }

    public void deleteFlight(Long id) {
        flightService.deleteFlight(id);
    }

}
