package cz.muni.fi.pa165.flights.controller;

import cz.muni.fi.pa165.flights.facade.FlightFacade;
import cz.muni.fi.pa165.generated.api.FlightsApiDelegate;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/flights")
public class FlightController implements FlightsApiDelegate {

    private final FlightFacade flightFacade;

    @Autowired
    public FlightController(FlightFacade flightFacade) {
        this.flightFacade = flightFacade;
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Create new flight"
    )
    @PostMapping
    public ResponseEntity<FlightDTO> createFlight(@RequestBody FlightDTO flightDTO) {
        return new ResponseEntity<>(flightFacade.createFlight(flightDTO), HttpStatus.CREATED);
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "List all flights with possibility to filter specified properties"
    )
    @GetMapping
    public ResponseEntity<List<FlightDTO>> listFlights(@RequestParam(required = false) OffsetDateTime departureAfter,
                                                       @RequestParam(required = false) OffsetDateTime arrivalBefore,
                                                       @RequestParam(required = false) Long departureDestination,
                                                       @RequestParam(required = false) Long finalDestination,
                                                       @RequestParam(required = false) Long plane) {
        return ResponseEntity.ok(flightFacade.listFlights(departureAfter, arrivalBefore, departureDestination, finalDestination, plane));
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Get flight with id"
    )
    @GetMapping(path = "/{id}")
    public ResponseEntity<FlightDTO> getFlight(@PathVariable("id") Long id) {
        return ResponseEntity.ok(flightFacade.findById(id));
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Update flight with id"
    )
    @PutMapping(path = "/{id}")
    public ResponseEntity<FlightDTO> updateFlight(@PathVariable("id") Long id, @RequestBody FlightDTO flightDTO) {
        return ResponseEntity.ok(flightFacade.updateFlight(flightDTO.id(id)));
    }

    @Override
    @Operation(
            security = @SecurityRequirement(name = "Bearer"),
            summary = "Delete flight with id"
    )
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteFlight(@PathVariable("id") Long id) {
        flightFacade.deleteFlight(id);
        return ResponseEntity.noContent().build();
    }
}
