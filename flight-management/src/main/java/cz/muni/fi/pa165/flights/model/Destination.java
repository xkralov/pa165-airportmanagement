package cz.muni.fi.pa165.flights.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Destination {

    private Long id;
    private String country;
    private String city;
    private String airportName;

    @Override
    public String toString() {
        return "Destination{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", airportName='" + airportName + '\'' +
                '}';
    }

}
