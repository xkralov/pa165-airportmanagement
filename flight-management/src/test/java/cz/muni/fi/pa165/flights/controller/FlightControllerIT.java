package cz.muni.fi.pa165.flights.controller;

import cz.muni.fi.pa165.flights.data.repository.FlightRepository;
import cz.muni.fi.pa165.flights.model.Flight;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.OffsetDateTime;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class FlightControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FlightRepository flightRepository;

    @Test
    void listFlights_returnsFlights() throws Exception {
        Flight flight = new Flight();
        flight.setDepartureTime(OffsetDateTime.parse("2024-04-25T12:30:00Z"));
        flight.setArrivalTime(OffsetDateTime.parse("2024-04-25T13:30:00Z"));
        flight.setOriginDestinationId(1L);
        flight.setFinalDestinationId(2L);
        flight.setPlaneId(1L);
        flightRepository.save(flight);

        mockMvc.perform(MockMvcRequestBuilders.get("/flights")
                        .param("departureAfter", "2024-04-25T12:00:00Z")
                        .param("arrivalBefore", "2024-04-25T14:00:00Z")
                        .param("departureDestination", "1")
                        .param("finalDestination", "2")
                        .param("plane", "1")
                        .contentType(MediaType.APPLICATION_JSON))


                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].departureTime").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].arrivalTime").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].originDestinationId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].finalDestinationId").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].planeId").value(1));
    }
}
