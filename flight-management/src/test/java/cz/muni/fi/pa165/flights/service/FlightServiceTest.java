package cz.muni.fi.pa165.flights.service;

import cz.muni.fi.pa165.flights.exceptions.DataStorageException;
import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.flights.mappers.FlightMapper;
import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.flights.data.repository.FlightRepository;
import cz.muni.fi.pa165.flights.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private ExternalServiceClient externalServiceClient;

    @InjectMocks
    private FlightService flightService;


    @Test
    void listFlights_repositoryReturnsFlights_returnsFlights() {
        List<Flight> flights = Arrays.asList(new Flight(), new Flight());
        when(flightRepository.findAllByAttributes(FlightService.MIN_DATETIME, FlightService.MAX_DATETIME, null, null, null)).thenReturn(flights);

        List<Flight> result = flightService.listFlights(null, null, null, null, null);

        assertEquals(flights, result);
        verify(flightRepository, times(1)).findAllByAttributes(FlightService.MIN_DATETIME, FlightService.MAX_DATETIME, null, null, null);
    }

    @Test
    void listFlights_noFlightFound_returnsEmpty() {
        List<Flight> flights = List.of();
        var departure = OffsetDateTime.now().minusDays(7);
        var arrival = OffsetDateTime.now().minusDays(7).plusHours(2);
        when(flightRepository.findAllByAttributes(departure, arrival, null, null, null))
                .thenReturn(flights);

        List<Flight> result = flightRepository.findAllByAttributes(departure, arrival, null, null, null);

        assertEquals(flights, result);
        verify(flightRepository, times(1)).findAllByAttributes(departure, arrival, null, null, null);
    }

    @Test
    void findById_flightExists_returnsFlight() {
        Long id = 1L;
        Flight flight = new Flight();
        when(flightRepository.findById(id)).thenReturn(Optional.of(flight));

        Flight result = flightService.findById(id);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).findById(id);
    }

    @Test
    void findById_flightDoesNotExist_throwsResourceNotFoundException() {
        Long id = 1L;
        when(flightRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> flightService.findById(id));
        verify(flightRepository, times(1)).findById(id);
    }

    @Test
    void createFlight_validFlight_createsFlight() {
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.save(flight)).thenReturn(flight);
        when(flightRepository.findOverlappingFlights(any(OffsetDateTime.class), any(OffsetDateTime.class), any(Long.class), any(Long.class))).thenReturn(new ArrayList<>());
        doNothing().when(externalServiceClient).airplaneExists(any());
        doNothing().when(externalServiceClient).destinationExists(any());

        Flight result = flightService.createFlight(flight);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void createFlight_airplaneNotAvailable_throwsDataStorageException() {
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.findOverlappingFlights(any(OffsetDateTime.class), any(OffsetDateTime.class), any(Long.class), any())).thenReturn(List.of(new Flight()));
        doNothing().when(externalServiceClient).airplaneExists(any());
        doNothing().when(externalServiceClient).destinationExists(any());

        assertThrows(DataStorageException.class, () -> flightService.createFlight(flight));
    }

    @Test
    void updateFlight_validFlight_updatesFlight() {
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.save(flight)).thenReturn(flight);
        when(flightRepository.findById(flight.getId())).thenReturn(Optional.of(flight));
        when(flightRepository.findOverlappingFlights(any(OffsetDateTime.class), any(OffsetDateTime.class), any(Long.class), any())).thenReturn(new ArrayList<>());
        doNothing().when(externalServiceClient).airplaneExists(any());
        doNothing().when(externalServiceClient).destinationExists(any());

        Flight result = flightService.updateFlight(flight);

        assertEquals(flight, result);
        verify(flightRepository, times(1)).save(flight);
    }

    @Test
    void updateFlight_airplaneNotAvailable_throwsDataStorageException() {
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.findById(flight.getId())).thenReturn(Optional.of(flight));
        when(flightRepository.findOverlappingFlights(any(OffsetDateTime.class), any(OffsetDateTime.class), any(Long.class), any())).thenReturn(List.of(new Flight()));
        doNothing().when(externalServiceClient).airplaneExists(any());
        doNothing().when(externalServiceClient).destinationExists(any());

        assertThrows(DataStorageException.class, () -> flightService.updateFlight(flight));
    }

    @Test
    void updateFlight_flightDoesNotExist_throwsResourceNotFoundException() {
        Flight flight = TestDataFactory.flightEntity;
        when(flightRepository.findById(flight.getId())).thenReturn(Optional.empty());
        doNothing().when(externalServiceClient).airplaneExists(any());
        doNothing().when(externalServiceClient).destinationExists(any());

        assertThrows(ResourceNotFoundException.class, () -> flightService.updateFlight(flight));
        verify(flightRepository, times(0)).save(any(Flight.class));
    }

    @Test
    void deleteFlight_flightExists_deletesFlight() {
        Long id = 1L;
        doNothing().when(flightRepository).deleteById(id);
        when(flightRepository.findById(id)).thenReturn(Optional.of(new Flight()));

        flightService.deleteFlight(id);

        verify(flightRepository, times(1)).deleteById(id);
    }

    @Test
    void deleteFlight_flightDoesNotExists_throwsResourceNotFoundException() {
        Long id = 1L;
        when(flightRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> flightService.deleteFlight(id));

        verify(flightRepository, times(0)).deleteById(id);
    }
}
