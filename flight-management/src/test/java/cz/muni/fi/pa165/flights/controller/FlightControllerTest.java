package cz.muni.fi.pa165.flights.controller;

import cz.muni.fi.pa165.flights.facade.FlightFacade;
import cz.muni.fi.pa165.flights.util.TestDataFactory;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class FlightControllerTest {

    @Mock
    private FlightFacade flightFacade;

    @InjectMocks
    private FlightController flightController;


    @Test
    void createFlight_successful_returnsFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        when(flightFacade.createFlight(any())).thenReturn(flightDTO);

        ResponseEntity<FlightDTO> response = flightController.createFlight(flightDTO);

        assertEquals(201, response.getStatusCode().value());
        assertEquals(flightDTO, response.getBody());
        verify(flightFacade, times(1)).createFlight(any());
    }

    @Test
    void listFlights_successful_returnsFlights() {
        when(flightFacade.listFlights(null, null, null, null, null)).thenReturn(Collections.singletonList(TestDataFactory.flightDTO));

        ResponseEntity<List<FlightDTO>> response = flightController.listFlights(null, null, null, null, null);

        assertEquals(200, response.getStatusCode().value());
        assertEquals(Collections.singletonList(TestDataFactory.flightDTO), response.getBody());
        verify(flightFacade, times(1)).listFlights(null, null, null, null, null);
    }

    @Test
    void listFlights_noFlightFound_returnsEmpty() {
        var departure = OffsetDateTime.now().minusDays(7);
        var arrival = OffsetDateTime.now().minusDays(7).plusHours(2);
        when(flightFacade.listFlights(departure, arrival, null, null, null)).thenReturn(Collections.emptyList());

        ResponseEntity<List<FlightDTO>> response = flightController.listFlights(departure, arrival, null, null, null);

        assertEquals(200, response.getStatusCode().value());
        assertEquals(Collections.emptyList(), response.getBody());
        verify(flightFacade, times(1)).listFlights(departure, arrival, null, null, null);
    }

    @Test
    void getFlight_successful_returnsFlight() {
        when(flightFacade.findById(anyLong())).thenReturn(TestDataFactory.flightDTO);

        ResponseEntity<FlightDTO> response = flightController.getFlight(1L);

        assertEquals(200, response.getStatusCode().value());
        assertEquals(TestDataFactory.flightDTO, response.getBody());
        verify(flightFacade, times(1)).findById(anyLong());
    }

    @Test
    void updateFlight_successful_returnsFlight() {
        when(flightFacade.updateFlight(any())).thenReturn(TestDataFactory.flightDTO);

        ResponseEntity<FlightDTO> response = flightController.updateFlight(1L, TestDataFactory.flightDTO);

        assertEquals(200, response.getStatusCode().value());
        assertEquals(TestDataFactory.flightDTO, response.getBody());
        verify(flightFacade, times(1)).updateFlight(any());
    }

    @Test
    void deleteFlight_successful_returnsNoContent() {
        doNothing().when(flightFacade).deleteFlight(anyLong());

        ResponseEntity<Void> response = flightController.deleteFlight(1L);

        assertEquals(204, response.getStatusCode().value());
        verify(flightFacade, times(1)).deleteFlight(anyLong());
    }
}
