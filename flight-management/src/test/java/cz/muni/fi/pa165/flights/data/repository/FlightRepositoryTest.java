package cz.muni.fi.pa165.flights.data.repository;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.flights.service.FlightService;
import cz.muni.fi.pa165.flights.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.OffsetDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class FlightRepositoryTest {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @BeforeEach
    void initData() {
        Flight flight = TestDataFactory.flightEntity;
        testEntityManager.merge(flight);
    }

    @Test
    void findByAllAttributes_flightFound1_returnsFlight() {
        OffsetDateTime departure = FlightService.MIN_DATETIME;
        OffsetDateTime arrival = FlightService.MAX_DATETIME;

        List<Flight> flights = flightRepository.findAllByAttributes(departure, arrival, null, null, null);

        assertThat(flights).hasSize(1);
    }

    @Test
    void findByAllAttributes_flightFound2_returnsFlight() {
        OffsetDateTime departure = FlightService.MIN_DATETIME;
        OffsetDateTime arrival = FlightService.MAX_DATETIME;
        Long origin = TestDataFactory.flightEntity.getOriginDestinationId();
        Long dest = TestDataFactory.flightEntity.getFinalDestinationId();
        Long plane = TestDataFactory.flightEntity.getPlaneId();

        List<Flight> flights = flightRepository.findAllByAttributes(departure, arrival, origin, dest, plane);

        assertThat(flights).hasSize(1);
    }

    @Test
    void findByAllAttributes_noFlightFound_returnsEmpty() {
        OffsetDateTime departure = OffsetDateTime.now().minusDays(7);
        OffsetDateTime arrival = OffsetDateTime.now().minusDays(7).plusHours(2);

        List<Flight> flights = flightRepository.findAllByAttributes(departure, arrival, null, null, null);

        assertThat(flights).isEmpty();
    }

}
