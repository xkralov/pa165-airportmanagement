package cz.muni.fi.pa165.flights.facade;

import cz.muni.fi.pa165.flights.service.FlightService;
import cz.muni.fi.pa165.flights.mappers.FlightMapper;
import cz.muni.fi.pa165.flights.util.TestDataFactory;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import cz.muni.fi.pa165.flights.exceptions.DataStorageException;
import cz.muni.fi.pa165.flights.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightFacadeTest {

    @Mock
    private FlightService flightService;

    @Mock
    private FlightMapper flightMapper;

    @InjectMocks
    private FlightFacade flightFacade;


    @Test
    void listFlights_flightsExist_returnsMappedFlights() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        List<FlightDTO> flightDTOs = Collections.singletonList(flightDTO);
        when(flightService.listFlights(null, null, null, null, null)).thenReturn(Collections.singletonList(TestDataFactory.flightEntity));
        when(flightMapper.mapToList(anyList())).thenReturn(flightDTOs);

        List<FlightDTO> result = flightFacade.listFlights(null, null, null, null, null);

        assertEquals(flightDTOs, result);
        verify(flightService).listFlights(null, null, null, null, null);
        verify(flightMapper).mapToList(anyList());
    }

    @Test
    void listFlights_noFlightFound_returnsEmpty() {
        List<FlightDTO> flightDTOs = Collections.emptyList();
        OffsetDateTime departure = OffsetDateTime.now().minusDays(7);
        OffsetDateTime arrival = OffsetDateTime.now().minusDays(7).plusHours(2);
        when(flightService.listFlights(departure, arrival, null, null, null)).thenReturn(Collections.emptyList());
        when(flightMapper.mapToList(anyList())).thenReturn(flightDTOs);

        List<FlightDTO> result = flightFacade.listFlights(
                departure,
                arrival,
                null,
                null,
                null);

        assertEquals(flightDTOs, result);
        verify(flightService, times(1)).listFlights(
                departure,
                arrival,
                null,
                null,
                null);
    }

    @Test
    void findById_flightExists_returnsMappedFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        when(flightService.findById(anyLong())).thenReturn(TestDataFactory.flightEntity);
        when(flightMapper.mapToDto(any())).thenReturn(flightDTO);

        FlightDTO result = flightFacade.findById(1L);

        assertEquals(flightDTO, result);
        verify(flightService).findById(anyLong());
        verify(flightMapper).mapToDto(any());
    }

    @Test
    void findById_flightDoesNotExist_throwsResourceNotFoundException() {
        when(flightService.findById(anyLong())).thenThrow(new ResourceNotFoundException("Flight with id: " + 1L + " was not found"));

        assertThrows(ResourceNotFoundException.class, () -> flightFacade.findById(1L));
        verify(flightService).findById(anyLong());
    }

    @Test
    void createFlight_successful_returnsMappedFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        when(flightService.createFlight(any())).thenReturn(TestDataFactory.flightEntity);
        when(flightMapper.mapToDto(any())).thenReturn(flightDTO);

        FlightDTO result = flightFacade.createFlight(flightDTO);

        assertEquals(flightDTO, result);
        verify(flightService).createFlight(any());
        verify(flightMapper).mapToDto(any());
    }

    @Test
    void createFlight_serviceThrowsDataStorageException_throwsRuntimeException() {
        when(flightService.createFlight(any())).thenThrow(new DataStorageException("Airplane with id: " + 1L + " does not exist"));

        assertThrows(RuntimeException.class, () -> flightFacade.createFlight(TestDataFactory.flightDTO));
        verify(flightService).createFlight(any());
    }

    @Test
    void updateFlight_successful_returnsMappedFlight() {
        FlightDTO flightDTO = TestDataFactory.flightDTO;
        when(flightService.updateFlight(any())).thenReturn(TestDataFactory.flightEntity);
        when(flightMapper.mapToDto(any())).thenReturn(flightDTO);

        FlightDTO result = flightFacade.updateFlight(flightDTO);

        assertEquals(flightDTO, result);
        verify(flightService).updateFlight(any());
        verify(flightMapper).mapToDto(any());
    }

    @Test
    void updateFlight_serviceThrowsDataStorageException_throwsRuntimeException() {
        when(flightService.updateFlight(any())).thenThrow(new DataStorageException("Airplane with id: " + 1L + " does not exist"));

        assertThrows(RuntimeException.class, () -> flightFacade.updateFlight(TestDataFactory.flightDTO));
        verify(flightService).updateFlight(any());
    }

    @Test
    void deleteFlight_successful_callsDeleteFlight() {
        doNothing().when(flightService).deleteFlight(anyLong());

        flightFacade.deleteFlight(1L);

        verify(flightService).deleteFlight(anyLong());
    }
}
