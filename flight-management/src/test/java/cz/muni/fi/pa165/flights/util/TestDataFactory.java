package cz.muni.fi.pa165.flights.util;

import cz.muni.fi.pa165.flights.model.Flight;
import cz.muni.fi.pa165.generated.model.FlightDTO;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
public class TestDataFactory {

    private static final Long id = 1L;
    private static final OffsetDateTime departureTime = OffsetDateTime.now();
    private static final OffsetDateTime arrivalTime = OffsetDateTime.now().plusHours(2);
    private static final Long originDestinationId = 1L;
    private static final Long finalDestinationId = 2L;
    private static final Long airplaneId = 1L;

    public static FlightDTO flightDTO = getFlightDTOFactory();
    public static Flight flightEntity = getFlightEntity();

    private static FlightDTO getFlightDTOFactory() {
        return new FlightDTO(departureTime, arrivalTime, originDestinationId, finalDestinationId, airplaneId).id(id);
    }

    private static Flight getFlightEntity() {
        var flight = new Flight();
        flight.setId(id);
        flight.setDepartureTime(departureTime);
        flight.setArrivalTime(arrivalTime);
        flight.setOriginDestinationId(originDestinationId);
        flight.setFinalDestinationId(finalDestinationId);
        flight.setPlaneId(airplaneId);
        return flight;
    }
}
