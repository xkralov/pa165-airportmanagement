import json
import string
import random

from locust import HttpUser, task, between

AUTH_TOKEN = ""

HEADER = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + AUTH_TOKEN
}

def random_word():
    return "".join(random.choice(string.ascii_letters) for _ in range(10))

class User(HttpUser) :
    host = "http://localhost:8080"
    wait_time = between(1, 3)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = "http://localhost:8080"
        self.flight_service = "http://localhost:8081"
        self.resources_service = "http://localhost:8082"
        self.flights = []
        self.destinations = []

    @task
    def get_all_flights(self):
        self.client.base_url = self.resources_service
        response = self.client.get("http://localhost:8081/flights")
        if (response.status_code != 200):
            print("Flight fetch failed")
        else:
            self.destinations.append(response.json())

    @task
    def get_all_destinations(self):
        self.client.base_url = self.resources_service
        response = self.client.get("http://localhost:8082/destinations", headers=HEADER)
        if (response.status_code != 200):
            print("Destinations fetch failed")
        else:
            self.destinations.append(response.json())

    @task
    def get_destination_by_id(self):
        if len(self.destinations) == 0:
            print("No destinations")
            return

        self.client.base_url = self.resources_service
        dest_id = self.destinations[0][0]["id"]
        response = self.client.get("http://localhost:8082/destinations/" + str(dest_id), headers=HEADER)
        if (response.status_code != 200):
            print("Destination fetch failed")
        else:
            print("Resp: " + str(response.json()))


class Admin(HttpUser):
    host = "http://localhost:8080"
    wait_time = between(1, 3)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.host = "http://localhost:8080"
        self.resources_service = "http://localhost:8082"
        self.flight_service = "http://localhost:8081"
        self.steward_service = "http://localhost:8080"
        self.destinations = []
        self.planes = []
        self.flights = []
        self.stewards = []
        self.headers = {}

    @task
    def create_destination(self):
        print("------ Create destination -------")
        self.client.base_url = self.resources_service

        response = self.client.post("http://localhost:8082/destinations", headers=HEADER, catch_response=True, data=json.dumps({
                "id": 0,
                "country": random_word(),
                "city": random_word(),
                "airportName": random_word()
            }))

        if (response.status_code != 200):
            print(response.json())
            print("Destination create failed")
        else:
            self.destinations.append(response.json())
            print("Resp: " + str(response.json()))

    @task
    def create_airplane(self):
        print("------ Create Airplane -------")
        self.client.base_url = self.resources_service

        response = self.client.post("http://localhost:8082/airplanes", headers=HEADER, catch_response=True, data=json.dumps({
                "id": 0,
                "name": random_word(),
                "type": random_word(),
                "capacity": 100
            }))

        if (response.status_code != 200):
            print(response.json())
            print("Plane create failed")
        else:
            self.planes.append(response.json())
            print("Resp: " + str(response.json()))

    @task
    def create_steward(self):
        print("------ Create steward -------")
        self.client.base_url = self.steward_service

        response = self.client.post("http://localhost:8080/stewards", headers=HEADER, catch_response=True, data=json.dumps({
                "firstName": random_word(),
                "lastName": random_word()
            }))

        if (response.status_code != 201):
            print(response.json())
            print("Steward create failed")
        else:
            self.stewards.append(response.json())
            print("Resp: " + str(response.json()))

    @task
    def create_flight(self):
        print("------ Create flight -------")

        if len(self.destinations) < 2 or len(self.planes) < 1 or len(self.stewards) == 0:
            print("Not enough resources to create flight")
            return

        self.client.base_url = self.flight_service

        dep_dest_id = random.choice(self.destinations)["id"]
        arr_dest_id = random.choice(self.destinations)["id"]
        plane_id = random.choice(self.planes)["id"]

        response = self.client.post("http://localhost:8081/flights", headers=HEADER, catch_response=True, data=json.dumps({
                "id": 0,
                "departureTime": "2024-12-31T12:30:00Z",
                "arrivalTime": "2024-12-31T14:30:00Z",
                "originDestinationId": dep_dest_id,
                "finalDestinationId": arr_dest_id,
                "planeId": plane_id
            }))

        if (response.status_code != 200):
            print(response.json())
            print("Flight create failed")
        else:
            self.flights.append(response.json())
            print("Resp: " + str(response.json()))

    @task
    def assign_steward_to_flight(self):
        print("------ Assign steward to flight -------")

        if len(self.stewards) == 0 or len(self.flights) == 0:
            print("Not enough resources to assign steward to flight")
            return

        self.client.base_url = self.steward_service
        steward_id = random.choice(self.stewards)["id"]
        flight_id = random.choice(self.flights)["id"]
        uri = "http://localhost:8080/stewards/steward-to-flight/" + str(steward_id) + "/" + str(flight_id)

        response = self.client.put(uri, headers=HEADER, catch_response=True)

        if (response.status_code != 200):
            print(response.json())
            print("Steward assignment failed")
        else:
            self.stewards.append(response.json())
            print("Resp: " + str(response.json()))
